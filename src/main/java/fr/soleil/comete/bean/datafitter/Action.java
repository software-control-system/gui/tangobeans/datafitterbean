/**
 *
 */
package fr.soleil.comete.bean.datafitter;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.prefs.Preferences;

import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.StringScalarBox;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.StringButton;

/**
 * Action Panel for Datafitter
 * 
 * @author AMESYS
 * 
 */
public class Action extends AbstractTangoBox {

    private static final long serialVersionUID = -7472196338646811538L;

    private StringButton startButtonViewer = null;
    private static final String COMMANDE = "StartFit";
    protected final static String START_INFO = Messages.getString("Action.START_INFO");
    private final StringScalarBox stringBox;

    /**
     * This method initializes
     * 
     */
    public Action() {
        super();
        stringBox = (StringScalarBox) CometeBoxProvider.getCometeBox(StringScalarBox.class);
        initialize();
    }

    /**
     * This method initializes this
     * 
     */
    private void initialize() {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1.0D;
        gridBagConstraints.weighty = 0.0D;
        gridBagConstraints.insets = new Insets(5, 5, 5, 5);
        this.setLayout(new GridBagLayout());
        this.setSize(new Dimension(171, 42));
        this.add(getStartButtonViewer(), gridBagConstraints);

        this.setBorder(new TitledBorder(new LineBorder(DataFitterBean.BORDER_COLOR, 1),
                Messages.getString("Action.BORDER_TITLE"), TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), DataFitterBean.TITLE_COLOR));
    }

    /* (non-Javadoc)
     * @see fr.soleil.bean.AbstractBean#clearGUI()
     */
    @Override
    protected void clearGUI() {
        cleanWidget(getStartButtonViewer());
        getStartButtonViewer().setText(START_INFO);
    }

    /* (non-Javadoc)
     * @see fr.soleil.bean.AbstractBean#refreshGUI()
     */
    @Override
    protected void refreshGUI() {
        setWidgetModel(getStartButtonViewer(), stringBox, generateCommandKey(Action.COMMANDE));
        getStartButtonViewer().setText(START_INFO);
    }

    /**
     * This method initializes startButtonViewer
     * 
     * @return fr.esrf.tangoatk.widget.command.SimpleCommandButtonViewer
     */
    private StringButton getStartButtonViewer() {
        if (startButtonViewer == null) {
            startButtonViewer = generateStringButton();
            startButtonViewer.setText(START_INFO);
        }
        return startButtonViewer;
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void onConnectionError() {
        DataFitterBean.LOGGER.info("Action component, Connection error.");
    }

}