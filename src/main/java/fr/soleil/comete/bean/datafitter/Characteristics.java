package fr.soleil.comete.bean.datafitter;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.prefs.Preferences;

import javax.swing.JLabel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.service.ISourceListener;
import fr.soleil.comete.swing.TextField;
import fr.soleil.lib.project.ObjectUtils;

/**
 * @author HARDION.
 */
public class Characteristics extends AbstractTangoBox implements ISourceListener<String> {

    private static final long serialVersionUID = 7073627282426231056L;

    /**
     * positionLabel.
     */
    private JLabel positionLabel = null;
    private TextField positionViewer = null;
    protected final static String POSITION_INFO = Messages.getString("Characteristics.POSITION_INFO");
    protected final static String POSITION_TITLE = Messages.getString("Characteristics.POSITION_TITLE");
    private final static String POSITION_ATTRIBUTE = "position";

    private JLabel widthLabel = null;
    private TextField widthViewer = null;
    protected final static String WIDTH_INFO = Messages.getString("Characteristics.WIDTH_INFO");
    protected final static String WIDTH_TITLE = Messages.getString("Characteristics.WIDTH_TITLE");
    private final static String WIDTH_ATTRIBUTE = "width";

    private JLabel heightLabel = null;
    private TextField heightViewer = null;
    protected final static String HEIGHT_INFO = Messages.getString("Characteristics.HEIGHT_INFO");
    protected final static String HEIGHT_TITLE = Messages.getString("Characteristics.HEIGHT_TITLE");
    private final static String HEIGHT_ATTRIBUTE = "height";

    private JLabel backgroundLabel = null;
    private TextField backgroundViewer = null;
    protected final static String BACKGROUND_INFO = Messages.getString("Characteristics.BACKGROUND_INFO");
    protected final static String BACKGROUND_TITLE = Messages.getString("Characteristics.BACKGROUND_TITLE");
    private final static String BACKGROUND_ATTRIBUTE = "background";

    private JLabel hwhmLabel = null;
    private TextField hwhmViewer = null;
    protected final static String HWHM_INFO = Messages.getString("Characteristics.HWHM_INFO");
    protected final static String HWHM_TITLE = Messages.getString("Characteristics.HWHM_TITLE");
    private final static String HWHM_ATTRIBUTE = "hwhm";

    private JLabel fwhmLabel = null;
    private TextField fwhmViewer = null;
    protected final static String FWHM_INFO = Messages.getString("Characteristics.FWHM_INFO");
    protected final static String FWHM_TITLE = Messages.getString("Characteristics.FWHM_TITLE");
    private final static String FWHM_ATTRIBUTE = "fwhm";

    private JLabel xLowLabel = null;
    private TextField xLowViewer = null;
    protected final static String X_LOW_INFO = Messages.getString("Characteristics.XLOW_INFO");
    protected final static String X_LOW_TITLE = Messages.getString("Characteristics.XLOW_TITLE");
    private final static String XLOW_ATTRIBUTE = "xLow";

    private JLabel xHighLabel = null;
    private TextField xHighViewer = null;
    protected final static String XHIGH_INFO = Messages.getString("Characteristics.XHIGH_INFO");
    protected final static String XHIGH_TITLE = Messages.getString("Characteristics.XHIGH_TITLE");
    private final static String XHIGH_ATTRIBUTE = "xHigh";

    private JLabel aBackgroundLabel = null;
    private TextField aBackgroundViewer = null;
    protected final static String A_BACKGROUND_INFO = Messages.getString("Characteristics.A_BACKGROUND_INFO");
    protected final static String A_BACKGROUND_TITLE = Messages.getString("Characteristics.A_BACKGROUND_TITLE");
    private final static String A_BACKGROUND_ATTRIBUTE = "backgroundA";

    private JLabel bBackgroundLabel = null;
    private TextField bBackgroundViewer = null;
    protected final static String B_BACKGROUND_INFO = Messages.getString("Characteristics.B_BACKGROUND_INFO");
    protected final static String B_BACKGROUND_TITLE = Messages.getString("Characteristics.B_BACKGROUND_TITLE");
    private final static String B_BACKGROUND_ATTRIBUTE = "backgroundB";

    // Type of function last value
    private String lastValue;
    private static final String FUNCTION_TYPE_ATTRIBUTE = "fittingFunctionType";

    private GridBagConstraints xLowLabelConstraints = null;
    private GridBagConstraints xLowViewerConstraints = null;
    private GridBagConstraints xHighLabelConstraints = null;
    private GridBagConstraints xHighViewerConstraints = null;
    private GridBagConstraints backgroundLabelConstraints = null;
    private GridBagConstraints backgroundViewerConstraints = null;
    private GridBagConstraints aBackgroundLabelConstraints = null;
    private GridBagConstraints aBackgroundViewerConstraints = null;
    private GridBagConstraints bBackgroundLabelConstraints = null;
    private GridBagConstraints bBackgroundViewerConstraints = null;

    /**
     * This method initializes
     * 
     */
    public Characteristics() {
        super();
        initialize();
    }

    /**
     * This method initializes this
     * 
     */
    private void initialize() {

        lastValue = ObjectUtils.EMPTY_STRING;

        positionLabel = new JLabel();
        positionLabel.setHorizontalAlignment(JLabel.CENTER);
        positionLabel.setText(POSITION_TITLE);
        positionLabel.setToolTipText(POSITION_INFO);

        widthLabel = new JLabel();
        widthLabel.setHorizontalAlignment(JLabel.CENTER);
        widthLabel.setText(WIDTH_TITLE);
        widthLabel.setToolTipText(WIDTH_INFO);

        heightLabel = new JLabel();
        heightLabel.setHorizontalAlignment(JLabel.CENTER);
        heightLabel.setText(HEIGHT_TITLE);
        heightLabel.setToolTipText(HEIGHT_INFO);

        backgroundLabel = new JLabel();
        backgroundLabel.setHorizontalAlignment(JLabel.CENTER);
        backgroundLabel.setText(BACKGROUND_TITLE);
        backgroundLabel.setToolTipText(BACKGROUND_INFO);

        hwhmLabel = new JLabel();
        hwhmLabel.setHorizontalAlignment(JLabel.CENTER);
        hwhmLabel.setText(HWHM_TITLE);
        hwhmLabel.setToolTipText(HWHM_INFO);

        fwhmLabel = new JLabel();
        fwhmLabel.setHorizontalAlignment(JLabel.CENTER);
        fwhmLabel.setText(FWHM_TITLE);
        fwhmLabel.setToolTipText(FWHM_INFO);

        xLowLabel = new JLabel();
        xLowLabel.setHorizontalAlignment(JLabel.CENTER);
        xLowLabel.setText(X_LOW_TITLE);
        xLowLabel.setToolTipText(X_LOW_INFO);

        xHighLabel = new JLabel();
        xHighLabel.setHorizontalAlignment(JLabel.CENTER);
        xHighLabel.setText(XHIGH_TITLE);
        xHighLabel.setToolTipText(XHIGH_INFO);

        aBackgroundLabel = new JLabel();
        aBackgroundLabel.setHorizontalAlignment(JLabel.CENTER);
        aBackgroundLabel.setText(A_BACKGROUND_TITLE);
        aBackgroundLabel.setToolTipText(A_BACKGROUND_INFO);

        bBackgroundLabel = new JLabel();
        bBackgroundLabel.setHorizontalAlignment(JLabel.CENTER);
        bBackgroundLabel.setText(B_BACKGROUND_TITLE);
        bBackgroundLabel.setToolTipText(B_BACKGROUND_INFO);

        this.setLayout(new GridBagLayout());

        initializePosition();

        this.setBorder(new TitledBorder(new LineBorder(DataFitterBean.BORDER_COLOR, 1),
                Messages.getString("Characteristics.BORDER_TITLE"), //$NON-NLS-1$
                TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), //$NON-NLS-1$
                DataFitterBean.TITLE_COLOR));
    }

    /**
     * Define widget position.
     */
    private void initializePosition() {

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 0;
        this.add(positionLabel, constraints);

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.weightx = 1;
        this.add(getPositionViewer(), constraints);

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 1;
        this.add(widthLabel, constraints);

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.weightx = 1;
        this.add(getWidthViewer(), constraints);

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 2;
        this.add(heightLabel, constraints);

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = 2;
        constraints.weightx = 1;
        this.add(getHeightViewer(), constraints);

        this.add(backgroundLabel, getBackgroundLabelConstraints());

        this.add(getBackgroundViewer(), getBackgroundViewerConstraints());

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 4;
        this.add(hwhmLabel, constraints);

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = 4;
        constraints.weightx = 1;
        this.add(getHwhmViewer(), constraints);

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 5;
        this.add(fwhmLabel, constraints);

        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = 5;
        constraints.weightx = 1;
        this.add(getFwhmViewer(), constraints);

        this.add(xLowLabel, getXLowLabelConstraints());

        this.add(getXLowViewer(), getXLowViewerConstraints());

        this.add(xHighLabel, getXHighLabelConstraints());

        this.add(getXHighViewer(), getXHighViewerConstraints());

        this.add(aBackgroundLabel, getABackGroundLabelConstraints());

        this.add(getABackgroundViewer(), getABackgroundViewerConstraints());

        this.add(bBackgroundLabel, getBBackGroundLabelConstraints());

        this.add(getBBackgroundViewer(), getBBackgroundViewerConstraints());
    }

    private GridBagConstraints getXHighViewerConstraints() {
        if (xHighViewerConstraints == null) {
            xHighViewerConstraints = new GridBagConstraints();
            xHighViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            xHighViewerConstraints.gridx = 1;
            xHighViewerConstraints.gridy = 7;
            xHighViewerConstraints.weightx = 1;
        }
        return xHighViewerConstraints;
    }

    private GridBagConstraints getXHighLabelConstraints() {
        if (xHighLabelConstraints == null) {
            xHighLabelConstraints = new GridBagConstraints();
            xHighLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
            xHighLabelConstraints.gridx = 0;
            xHighLabelConstraints.gridy = 7;
        }
        return xHighLabelConstraints;
    }

    private GridBagConstraints getABackGroundLabelConstraints() {
        if (aBackgroundLabelConstraints == null) {
            aBackgroundLabelConstraints = new GridBagConstraints();
            aBackgroundLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
            aBackgroundLabelConstraints.gridx = 0;
            aBackgroundLabelConstraints.gridy = 8;
        }
        return aBackgroundLabelConstraints;
    }

    private GridBagConstraints getABackgroundViewerConstraints() {
        if (aBackgroundViewerConstraints == null) {
            aBackgroundViewerConstraints = new GridBagConstraints();
            aBackgroundViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            aBackgroundViewerConstraints.gridx = 1;
            aBackgroundViewerConstraints.gridy = 8;
            aBackgroundViewerConstraints.weightx = 1;
        }
        return aBackgroundViewerConstraints;
    }

    private GridBagConstraints getBBackgroundViewerConstraints() {
        if (bBackgroundViewerConstraints == null) {
            bBackgroundViewerConstraints = new GridBagConstraints();
            bBackgroundViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            bBackgroundViewerConstraints.gridx = 1;
            bBackgroundViewerConstraints.gridy = 9;
            bBackgroundViewerConstraints.weightx = 1;
        }
        return bBackgroundViewerConstraints;
    }

    private GridBagConstraints getBBackGroundLabelConstraints() {
        if (bBackgroundLabelConstraints == null) {
            bBackgroundLabelConstraints = new GridBagConstraints();
            bBackgroundLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
            bBackgroundLabelConstraints.gridx = 0;
            bBackgroundLabelConstraints.gridy = 9;
        }
        return bBackgroundLabelConstraints;
    }

    private GridBagConstraints getXLowLabelConstraints() {
        if (xLowLabelConstraints == null) {
            xLowLabelConstraints = new GridBagConstraints();
            xLowLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
            xLowLabelConstraints.gridx = 0;
            xLowLabelConstraints.gridy = 6;
        }
        return xLowLabelConstraints;
    }

    private GridBagConstraints getBackgroundViewerConstraints() {
        if (backgroundViewerConstraints == null) {
            backgroundViewerConstraints = new GridBagConstraints();
            backgroundViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            backgroundViewerConstraints.gridx = 1;
            backgroundViewerConstraints.gridy = 3;
            backgroundViewerConstraints.weightx = 1;
        }
        return backgroundViewerConstraints;
    }

    private GridBagConstraints getBackgroundLabelConstraints() {
        if (backgroundLabelConstraints == null) {
            backgroundLabelConstraints = new GridBagConstraints();
            backgroundLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
            backgroundLabelConstraints.gridx = 0;
            backgroundLabelConstraints.gridy = 3;
        }
        return backgroundLabelConstraints;
    }

    private GridBagConstraints getXLowViewerConstraints() {
        if (xLowViewerConstraints == null) {
            xLowViewerConstraints = new GridBagConstraints();
            xLowViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            xLowViewerConstraints.gridx = 1;
            xLowViewerConstraints.gridy = 6;
            xLowViewerConstraints.weightx = 1;
        }
        return xLowViewerConstraints;
    }

    /* (non-Javadoc)
     * @see fr.soleil.bean.AbstractTangoBean#refreshGUI()
     */
    @Override
    protected void refreshGUI() {

        setWidgetModel(getPositionViewer(), stringBox, generateAttributeKey(Characteristics.POSITION_ATTRIBUTE));
        setWidgetModel(getWidthViewer(), stringBox, generateAttributeKey(Characteristics.WIDTH_ATTRIBUTE));
        setWidgetModel(getHeightViewer(), stringBox, generateAttributeKey(Characteristics.HEIGHT_ATTRIBUTE));
        setWidgetModel(getBackgroundViewer(), stringBox, generateAttributeKey(Characteristics.BACKGROUND_ATTRIBUTE));
        setWidgetModel(getHwhmViewer(), stringBox, generateAttributeKey(Characteristics.HWHM_ATTRIBUTE));
        setWidgetModel(getFwhmViewer(), stringBox, generateAttributeKey(Characteristics.FWHM_ATTRIBUTE));
        setWidgetModel(getXLowViewer(), stringBox, generateAttributeKey(Characteristics.XLOW_ATTRIBUTE));
        setWidgetModel(getXHighViewer(), stringBox, generateAttributeKey(Characteristics.XHIGH_ATTRIBUTE));
        setWidgetModel(getABackgroundViewer(), stringBox, generateAttributeKey(Characteristics.A_BACKGROUND_ATTRIBUTE));
        setWidgetModel(getBBackgroundViewer(), stringBox, generateAttributeKey(Characteristics.B_BACKGROUND_ATTRIBUTE));

        // listen the type of function
        stringBox.addSourceListener(this, generateAttributeKey(Characteristics.FUNCTION_TYPE_ATTRIBUTE));

    }

    private TextField getBackgroundViewer() {
        if (backgroundViewer == null) {
            backgroundViewer = generateTextField();
            backgroundViewer.setToolTipText(BACKGROUND_INFO);
            stringBox.setReadOnly(backgroundViewer, true);
        }
        return backgroundViewer;
    }

    private TextField getHeightViewer() {
        if (heightViewer == null) {
            heightViewer = generateTextField();
            heightViewer.setToolTipText(HEIGHT_INFO);
            stringBox.setReadOnly(heightViewer, true);
        }
        return heightViewer;
    }

    private TextField getPositionViewer() {
        if (positionViewer == null) {
            positionViewer = generateTextField();
            positionViewer.setToolTipText(POSITION_INFO);
            stringBox.setReadOnly(positionViewer, true);
        }
        return positionViewer;
    }

    private TextField getWidthViewer() {
        if (widthViewer == null) {
            widthViewer = generateTextField();
            widthViewer.setToolTipText(WIDTH_INFO);
            stringBox.setReadOnly(widthViewer, true);
        }
        return widthViewer;
    }

    private TextField getFwhmViewer() {
        if (fwhmViewer == null) {
            fwhmViewer = generateTextField();
            fwhmViewer.setToolTipText(FWHM_INFO);
            stringBox.setReadOnly(fwhmViewer, true);
        }
        return fwhmViewer;
    }

    private TextField getHwhmViewer() {
        if (hwhmViewer == null) {
            hwhmViewer = generateTextField();
            hwhmViewer.setToolTipText(HWHM_INFO);
            stringBox.setReadOnly(hwhmViewer, true);
        }
        return hwhmViewer;
    }

    private TextField getXHighViewer() {
        if (xHighViewer == null) {
            xHighViewer = generateTextField();
            xHighViewer.setToolTipText(XHIGH_INFO);
            stringBox.setReadOnly(xHighViewer, true);
        }
        return xHighViewer;
    }

    private TextField getABackgroundViewer() {
        if (aBackgroundViewer == null) {
            aBackgroundViewer = generateTextField();
            aBackgroundViewer.setToolTipText(A_BACKGROUND_INFO);
            stringBox.setReadOnly(aBackgroundViewer, true);
        }
        return aBackgroundViewer;
    }

    private TextField getBBackgroundViewer() {
        if (bBackgroundViewer == null) {
            bBackgroundViewer = generateTextField();
            bBackgroundViewer.setToolTipText(B_BACKGROUND_INFO);
            stringBox.setReadOnly(bBackgroundViewer, true);
        }
        return bBackgroundViewer;
    }

    private TextField getXLowViewer() {
        if (xLowViewer == null) {
            xLowViewer = generateTextField();
            xLowViewer.setToolTipText(X_LOW_INFO);
            stringBox.setReadOnly(xLowViewer, true);
        }
        return xLowViewer;
    }

    private void updateDynamicFields() {
        // if SIGMOID
        if (lastValue.contains(Parameters.SIGMOID)) {
            if (xLowLabel.getParent() == null) {
                this.add(xLowLabel, getXLowLabelConstraints());
                this.add(getXLowViewer(), getXLowViewerConstraints());
                this.add(xHighLabel, getXHighLabelConstraints());
                this.add(getXHighViewer(), getXHighViewerConstraints());
            }
        } else {
            if (xLowLabel.getParent() != null) {
                this.remove(xLowLabel);
                this.remove(getXLowViewer());
                this.remove(xHighLabel);
                this.remove(getXHighViewer());
            }
        }
        // if end with b or MEANWITHTHESHOLD
        if (lastValue.trim().endsWith("b") || lastValue.equals(Parameters.MEANWITHTHESHOLD)) {
            if (backgroundLabel.getParent() == null) {
                this.add(backgroundLabel, getBackgroundLabelConstraints());
                this.add(getBackgroundViewer(), getBackgroundViewerConstraints());
            }
        } else {
            if (backgroundLabel.getParent() != null) {
                this.remove(backgroundLabel);
                this.remove(getBackgroundViewer());
            }
        }
        revalidate();
        repaint();
    }

    @Override
    public void onSourceChange(String data) {

        if (data != null && !data.equals(lastValue)) {
            lastValue = data;
            updateDynamicFields();
        }
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void clearGUI() {
        cleanWidget(getBackgroundViewer());
        cleanWidget(getFwhmViewer());
        cleanWidget(getHeightViewer());
        cleanWidget(getHwhmViewer());
        cleanWidget(getPositionViewer());
        cleanWidget(getWidthViewer());
        cleanWidget(getXHighViewer());
        cleanWidget(getXLowViewer());
        cleanWidget(getABackgroundViewer());
        cleanWidget(getBBackgroundViewer());

    }

    @Override
    protected void onConnectionError() {
        DataFitterBean.LOGGER.info("Characteristics component, Connection error.");

    }

}