/**
 *
 */
package fr.soleil.comete.bean.datafitter;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.prefs.Preferences;

import javax.swing.JLabel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.BooleanComboBox;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.WheelSwitch;

/**
 * @author HARDION
 * 
 */
public class Curve extends AbstractTangoBox {

    private static final long serialVersionUID = -3578043445796605723L;
    private TextField startingXViewer = null;
    private WheelSwitch startingXWheelEditor = null;
    protected final static String STARTINGX_INFO = Messages.getString("Curve.STARTINGX_INFO");
    protected final static String STARTINGX_TITLE = Messages.getString("Curve.STARTINGX_TITLE");
    private final static String STARTING_X_ATTRIBUTE = "startingX";

    private TextField resolutionViewer = null;
    private WheelSwitch resolutionWheelEditor = null;
    protected final static String RESOLUTION_INFO = Messages.getString("Curve.RESOLUTION_INFO");
    protected final static String RESOLUTION_TITLE = Messages.getString("Curve.RESOLUTION_TITLE");
    private final static String RESOLUTION_X_ATTRIBUTE = "resolutionX";

    private CheckBox fittedDataSameSizeAsDataViewer = null;
    private BooleanComboBox fittedDataSameSizeAsDataEditor = null;
    protected final static String DATASAMESIZE_INFO = Messages.getString("Curve.DATASAMESIZE_INFO");
    protected final static String DATASAMESIZE_TITLE = Messages
            .getString("Curve.DATASAMESIZE_TITLE");
    private final static String FITTED_DATA_ATTRIBUTE = "fittedDataSameSizeAsData";

    private TextField pointsViewer = null;
    private WheelSwitch pointsWheelEditor = null;
    protected final static String POINTS_INFO = Messages.getString("Curve.POINTS_INFO");
    protected final static String POINTS_TITLE = Messages.getString("Curve.POINTS_TITLE");
    private final static String NB_POINTS_ATTRIBUTE = "nbPointsToGenerate";

    private final BooleanScalarBox booleanBox;

    /**
     * This method initializes
     * 
     */
    public Curve() {
        super();
        booleanBox = (BooleanScalarBox) CometeBoxProvider.getCometeBox(BooleanScalarBox.class);
        initialize();
    }

    /**
     * This method initializes this
     * 
     */
    private void initialize() {

        GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
        gridBagConstraints11.gridx = 2;
        gridBagConstraints11.gridy = 3;
        GridBagConstraints gridBagConstraints10 = new GridBagConstraints();
        gridBagConstraints10.gridx = 1;
        gridBagConstraints10.gridy = 3;
        GridBagConstraints gridBagConstraints9 = new GridBagConstraints();
        gridBagConstraints9.gridx = 0;
        gridBagConstraints9.gridy = 3;
        GridBagConstraints gridBagConstraints8 = new GridBagConstraints();
        gridBagConstraints8.gridx = 2;
        gridBagConstraints8.gridy = 2;
        GridBagConstraints gridBagConstraints7 = new GridBagConstraints();
        gridBagConstraints7.gridx = 2;
        gridBagConstraints7.gridy = 1;
        GridBagConstraints gridBagConstraints6 = new GridBagConstraints();
        gridBagConstraints6.gridx = 2;
        gridBagConstraints6.gridy = 0;
        GridBagConstraints gridBagConstraints5 = new GridBagConstraints();
        gridBagConstraints5.gridx = 1;
        gridBagConstraints5.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints5.gridy = 2;
        GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
        gridBagConstraints4.gridx = 1;
        gridBagConstraints4.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints4.gridy = 1;
        GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
        gridBagConstraints3.gridx = 1;
        gridBagConstraints3.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints3.gridy = 0;
        GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.gridx = 0;
        gridBagConstraints2.gridy = 2;
        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 1;
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;

        JLabel pointsLabel = new JLabel();
        pointsLabel.setText(POINTS_TITLE);
        pointsLabel.setToolTipText(POINTS_INFO);

        JLabel resolutionLabel = new JLabel();
        resolutionLabel.setText(RESOLUTION_TITLE);
        resolutionLabel.setToolTipText(RESOLUTION_INFO);

        JLabel startingXLabel = new JLabel();
        startingXLabel.setText(STARTINGX_TITLE);
        startingXLabel.setToolTipText(STARTINGX_INFO);

        JLabel fittedDataSameSizeAsDataLabel = new JLabel();
        fittedDataSameSizeAsDataLabel.setText(DATASAMESIZE_TITLE);
        fittedDataSameSizeAsDataLabel.setToolTipText(DATASAMESIZE_INFO);

        this.setLayout(new GridBagLayout());
        this.setSize(new Dimension(235, 135));

        this.add(startingXLabel, gridBagConstraints);
        this.add(resolutionLabel, gridBagConstraints1);
        this.add(pointsLabel, gridBagConstraints2);
        this.add(getStartingXViewer(), gridBagConstraints3);
        this.add(getResolutionViewer(), gridBagConstraints4);
        this.add(getPointsViewer(), gridBagConstraints5);

        this.add(getStartingXWheelEditor(), gridBagConstraints6);
        this.add(getResolutionWheelEditor(), gridBagConstraints7);
        this.add(getPointsWheelEditor(), gridBagConstraints8);
        this.add(fittedDataSameSizeAsDataLabel, gridBagConstraints9);
        this.add(getFittedDataSameSizeAsDataViewer(), gridBagConstraints10);
        this.add(getFittedDataSameSizeAsDataEditor(), gridBagConstraints11);

        getStartingXWheelEditor().setCometeFont(getStartingXViewer().getCometeFont());
        getResolutionWheelEditor().setCometeFont(getResolutionViewer().getCometeFont());
        getPointsWheelEditor().setCometeFont(getPointsViewer().getCometeFont());

        this.setBorder(new TitledBorder(new LineBorder(DataFitterBean.BORDER_COLOR, 1), Messages
                .getString("Curve.BORDER_TITLE"), TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12),
                DataFitterBean.TITLE_COLOR));

    }

    @Override
    protected void clearGUI() {
        cleanWidget(getFittedDataSameSizeAsDataEditor());
        cleanWidget(getFittedDataSameSizeAsDataViewer());
        cleanWidget(getPointsViewer());
        cleanWidget(getPointsWheelEditor());
        cleanWidget(getResolutionViewer());
        cleanWidget(getResolutionWheelEditor());
        cleanWidget(getStartingXViewer());
        cleanWidget(getStartingXWheelEditor());
    }

    @Override
    protected void onConnectionError() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void refreshGUI() {
        setWidgetModel(getStartingXViewer(), stringBox,
                generateAttributeKey(Curve.STARTING_X_ATTRIBUTE));
        setWidgetModel(getStartingXWheelEditor(), stringBox,
                generateWriteAttributeKey(Curve.STARTING_X_ATTRIBUTE));

        setWidgetModel(getResolutionViewer(), stringBox,
                generateAttributeKey(Curve.RESOLUTION_X_ATTRIBUTE));
        setWidgetModel(getResolutionWheelEditor(), stringBox,
                generateWriteAttributeKey(Curve.RESOLUTION_X_ATTRIBUTE));

        setWidgetModel(getPointsViewer(), stringBox,
                generateAttributeKey(Curve.NB_POINTS_ATTRIBUTE));
        setWidgetModel(getPointsWheelEditor(), stringBox,
                generateWriteAttributeKey(Curve.NB_POINTS_ATTRIBUTE));

        setWidgetModel(getFittedDataSameSizeAsDataViewer(), booleanBox,
                generateAttributeKey(Curve.FITTED_DATA_ATTRIBUTE));
        setWidgetModel(getFittedDataSameSizeAsDataEditor(), stringBox,
                generateWriteAttributeKey(Curve.FITTED_DATA_ATTRIBUTE));
    }

    /**
     * This method initializes startingXViewer
     * 
     * @return fr.esrf.tangoatk.widget.attribute.NumberScalarViewer
     */
    private TextField getStartingXViewer() {
        if (startingXViewer == null) {
            startingXViewer = generateTextField();
            startingXViewer.setToolTipText(STARTINGX_INFO);
            stringBox.setReadOnly(startingXViewer, true);
        }
        return startingXViewer;
    }

    /**
     * This method initializes resolutionViewer
     * 
     * @return fr.esrf.tangoatk.widget.attribute.NumberScalarViewer
     */
    private TextField getResolutionViewer() {
        if (resolutionViewer == null) {
            resolutionViewer = generateTextField();
            resolutionViewer.setToolTipText(RESOLUTION_INFO);
            stringBox.setReadOnly(resolutionViewer, true);
        }
        return resolutionViewer;
    }

    /**
     * This method initializes pointsViewer
     * 
     * @return fr.esrf.tangoatk.widget.attribute.NumberScalarViewer
     */
    private TextField getPointsViewer() {
        if (pointsViewer == null) {
            pointsViewer = generateTextField();
            pointsViewer.setToolTipText(POINTS_INFO);
            stringBox.setReadOnly(pointsViewer, true);
        }
        return pointsViewer;
    }

    /**
     * This method initializes startingXWheelEditor
     * 
     * @return fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor
     */
    private WheelSwitch getStartingXWheelEditor() {
        if (startingXWheelEditor == null) {
            startingXWheelEditor = generateWheelSwitch();
            startingXWheelEditor.setCometeBackground(new CometeColor(getBackground().getRed(),
                    getBackground().getGreen(), getBackground().getBlue()));
            startingXWheelEditor.setToolTipText(STARTINGX_INFO);
        }
        return startingXWheelEditor;
    }

    /**
     * This method initializes resolutionWheelEditor
     * 
     * @return fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor
     */
    private WheelSwitch getResolutionWheelEditor() {
        if (resolutionWheelEditor == null) {
            resolutionWheelEditor = generateWheelSwitch();
            resolutionWheelEditor.setCometeBackground(new CometeColor(getBackground().getRed(),
                    getBackground().getGreen(), getBackground().getBlue()));
            resolutionWheelEditor.setToolTipText(RESOLUTION_INFO);
        }
        return resolutionWheelEditor;
    }

    /**
     * This method initializes pointsWheelEditor
     * 
     * @return fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor
     */
    private WheelSwitch getPointsWheelEditor() {
        if (pointsWheelEditor == null) {
            pointsWheelEditor = generateWheelSwitch();
            pointsWheelEditor.setCometeBackground(new CometeColor(getBackground().getRed(),
                    getBackground().getGreen(), getBackground().getBlue()));
            pointsWheelEditor.setToolTipText(POINTS_INFO);
        }
        return pointsWheelEditor;
    }

    private CheckBox getFittedDataSameSizeAsDataViewer() {
        if (fittedDataSameSizeAsDataViewer == null) {
            fittedDataSameSizeAsDataViewer = generateCheckbox();
            fittedDataSameSizeAsDataViewer.setTrueLabel("true");
            fittedDataSameSizeAsDataViewer.setFalseLabel("false");
            fittedDataSameSizeAsDataViewer.setToolTipText(DATASAMESIZE_INFO);
        }
        return fittedDataSameSizeAsDataViewer;
    }

    private BooleanComboBox getFittedDataSameSizeAsDataEditor() {
        if (fittedDataSameSizeAsDataEditor == null) {
            fittedDataSameSizeAsDataEditor = generateBooleanComboBox();
            fittedDataSameSizeAsDataEditor.setToolTipText(DATASAMESIZE_INFO);
        }
        return fittedDataSameSizeAsDataEditor;
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

}