/*******************************************************************************
 * Copyright (c) 2006-2023 Synchrotron SOLEIL
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.All rights reserved. This program and
 * the accompanying materials
 *
 * Contributors:
 * vincent.hardion@synchrotron-soleil.fr - initial implementation
 *******************************************************************************/
package fr.soleil.comete.bean.datafitter;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.comete.awt.util.ColorTool;
import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.complexbox.ChartViewerBox;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.Chart;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TextField;
import fr.soleil.data.service.IKey;
import fr.soleil.lib.project.swing.ConstrainedCheckBox;
import fr.soleil.lib.project.swing.icons.Icons;

/**
 * Bean for DataFitter device
 * 
 * @author ABEILLE, HARDION
 * 
 */
public class DataFitterBean extends AbstractTangoBox {

    private static final long serialVersionUID = -5527688678797386476L;

    protected static final String NBDATA_INFO = Messages.getString("DataFitter.NBDATA_INFO");
    protected static final String NBDATA_TITLE = Messages.getString("DataFitter.NBDATA_TITLE");
    protected static final String NB_DATA_ATTRIBUTE = "nbData";
    protected static final String EQUATION_INFO = Messages.getString("DataFitter.EQUATION_INFO");
    protected static final String EQUATION_TITLE = Messages.getString("DataFitter.EQUATION_TITLE");
    protected static final String FONCTION_ATTRIBUTE = "functionEquation";
    protected static final String BEAN_PANEL = "beanPanel";
    protected static final String SPLIT_PANE = "splitPane";
    protected static final String ADVANCED_PANEL = "advancedPanel";
    protected static final String EXPERIMENTAL_DATA_X = "/experimentaldatax";
    protected static final String EXPERIMENTAL_DATA_Y = "/experimentaldatay";
    protected static final String FITTED_DATA_X = "/fitteddatax";
    protected static final String FITTED_DATA_Y = "/fitteddatay";

    protected static final Color TITLE_COLOR = new Color(50, 50, 150);
    protected static final Color BORDER_COLOR = new Color(200, 200, 255);

    protected static final Logger LOGGER = LoggerFactory.getLogger("logger");

    protected static final Dimension SIZE = new Dimension(704, 554);
    protected static final Dimension PREFERRED_SIZE = new Dimension(600, 400);

    private Chart fittedData;
    private JPanel parametersPanel;
    private JPanel deviceStatusBar;
    private JPanel beanPanel;
    private CardLayout mainPanelLayout;
    private Proxy advancedProxy;
    private Parameters parameters, advancedParameters;
    private Initial advancedInitial;
    private Action action, advancedAction;
    private Curve advancedCurve;
    private Quality quality, advancedQuality;
    private Characteristics characteristics, advancedCharacteristics;
    private JSplitPane splitPane;
    private ConstrainedCheckBox advancedModeCheckBox;
    private JPanel advancedPanel;
    private JPanel mainPanel;
    private JPanel fittedDataPanel;
    private JPanel infoPanel;
    private JLabel nbDataLabel;
    private TextField nbDataViewer;

    private JLabel functionEquationLabel;
    private Label functionEquationViewer;

    protected Icons ICON_BUNDLE;

    private final ChartViewerBox chartBox;

    public DataFitterBean() {
        super();
        chartBox = (ChartViewerBox) CometeBoxProvider.getCometeBox(ChartViewerBox.class);
        initialize();
        // Ask to automatically start and stop children beans according with this bean
        setManageChildrenStartStopAndRefresh(true);
    }

    /**
     * This method initializes this
     * 
     */
    private void initialize() {
        ICON_BUNDLE = createIconBundle();

        setLayout(new BorderLayout());
        setSize(SIZE);
        setPreferredSize(PREFERRED_SIZE);
        add(getBeanPanel(), BorderLayout.CENTER);
    }

    /**
     * This method initializes parametersPanel
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getParametersPanel() {
        if (parametersPanel == null) {

            Insets margin = new Insets(3, 3, 3, 3);

            GridBagConstraints parametersConstraints = new GridBagConstraints();
            parametersConstraints.fill = GridBagConstraints.BOTH;
            parametersConstraints.gridx = 0;
            parametersConstraints.gridy = 0;
            parametersConstraints.weightx = 1;
            parametersConstraints.weighty = 0.25;
            parametersConstraints.insets = margin;

            GridBagConstraints characteristicConstraints = new GridBagConstraints();
            characteristicConstraints.fill = GridBagConstraints.BOTH;
            characteristicConstraints.gridx = 0;
            characteristicConstraints.gridy = 1;
            characteristicConstraints.weightx = 1;
            characteristicConstraints.weighty = 0.25;
            characteristicConstraints.insets = margin;

            GridBagConstraints qualityConstraints = new GridBagConstraints();
            qualityConstraints.fill = GridBagConstraints.BOTH;
            qualityConstraints.gridx = 0;
            qualityConstraints.gridy = 2;
            qualityConstraints.weightx = 1;
            qualityConstraints.weighty = 0.25;
            qualityConstraints.insets = margin;

            GridBagConstraints actionConstraints = new GridBagConstraints();
            actionConstraints.fill = GridBagConstraints.HORIZONTAL;
            actionConstraints.gridx = 0;
            actionConstraints.gridy = 3;
            actionConstraints.weightx = 1;
            actionConstraints.weighty = 0;
            actionConstraints.insets = margin;
            actionConstraints.gridwidth = GridBagConstraints.REMAINDER;

            parametersPanel = new JPanel();
            parametersPanel.setLayout(new GridBagLayout());
            parametersPanel.add(getParameters(), parametersConstraints);
            parametersPanel.add(getCharacteristics(), characteristicConstraints);
            parametersPanel.add(getQuality(), qualityConstraints);
            parametersPanel.add(getAction(), actionConstraints);
        }
        return parametersPanel;
    }

    public String[] getXProxiesChoice() {
        return getAdvancedProxy().getXProxiesChoice();
    }

    public void setXProxiesChoice(String... xProxiesChoice) {
        getAdvancedProxy().setXProxiesChoice(xProxiesChoice);
    }

    public String[] getYProxiesChoice() {
        return getAdvancedProxy().getYProxiesChoice();
    }

    public void setYProxiesChoice(String... yProxiesChoice) {
        getAdvancedProxy().setYProxiesChoice(yProxiesChoice);
    }

    /**
     * This method initializes deviceStatusBar
     * 
     * @return fr.soleil.salsa.view.DeviceStatusBar
     */
    private JPanel getDeviceStatusBar() {
        if (deviceStatusBar == null) {
            deviceStatusBar = new JPanel(new GridBagLayout());

            JButton refresh = new JButton("Refresh");
            GridBagConstraints startConstraints = new GridBagConstraints();
            startConstraints.fill = GridBagConstraints.BOTH;
            startConstraints.gridx = 0;
            startConstraints.gridy = 0;
            startConstraints.weightx = 0;
            startConstraints.weighty = 1;
            startConstraints.insets = new Insets(5, 0, 5, 5);
            deviceStatusBar.add(refresh, startConstraints);
            refresh.addActionListener((e) -> {
                start();
            });

            GridBagConstraints statusConstraints = new GridBagConstraints();
            statusConstraints.fill = GridBagConstraints.BOTH;
            statusConstraints.gridx = 1;
            statusConstraints.gridy = 0;
            statusConstraints.weightx = 1;
            statusConstraints.weighty = 1;
            statusConstraints.insets = new Insets(5, 0, 5, 5);
            deviceStatusBar.add(getStatusPanel(), statusConstraints);

            GridBagConstraints checkBoxConstraints = new GridBagConstraints();
            checkBoxConstraints.fill = GridBagConstraints.VERTICAL;
            checkBoxConstraints.gridx = 2;
            checkBoxConstraints.gridy = 0;
            checkBoxConstraints.weightx = 0;
            checkBoxConstraints.weighty = 1;
            advancedModeCheckBox = new ConstrainedCheckBox(Messages.getString("DataFitter.ADVANCED_TITLE"));
            advancedModeCheckBox.setSelected(false);
            advancedModeCheckBox.addActionListener((e) -> {
                switchVisibility();
            });
            deviceStatusBar.add(advancedModeCheckBox, checkBoxConstraints);
        }
        return deviceStatusBar;
    }

    /**
     * This method initializes beanPanel
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getBeanPanel() {
        if (beanPanel == null) {
            beanPanel = new JPanel();
            beanPanel.setLayout(new BorderLayout());
            beanPanel.setName(BEAN_PANEL);
            beanPanel.setPreferredSize(new java.awt.Dimension(400, 400));
            beanPanel.add(getDeviceStatusBar(), java.awt.BorderLayout.SOUTH);
            beanPanel.add(getMainPanel(), BorderLayout.CENTER);

            repaint();
        }
        return beanPanel;
    }

    /**
     * This method initializes fittedData1
     * 
     * @return fr.esrf.tangoatk.widget.attribute.NonAttrNumberSpectrumViewer
     */
    private Chart getFittedData() {
        if (fittedData == null) {
            fittedData = new Chart();
            fittedData.setUseDisplayNameForDataSaving(true);
            fittedData.setAutoHighlightOnLegend(true);
            fittedData.setManagementPanelVisible(false);
        }
        return fittedData;
    }

    /**
     * This method initializes function
     * 
     * @return fr.soleil.bean.datafitter.Function
     */
    private Parameters getParameters() {
        if (parameters == null) {
            parameters = new Parameters();
        }
        return parameters;
    }

    /**
     * This method initializes action
     * 
     * @return fr.soleil.bean.datafitter.Action
     */
    private Action getAction() {
        if (action == null) {
            action = new Action();
        }
        return action;
    }

    /**
     * This method initializes splitPane
     * 
     * @return javax.swing.JSplitPane
     */
    private JSplitPane getSplitPane() {
        if (splitPane == null) {
            splitPane = new JSplitPane();
            splitPane.setName(SPLIT_PANE);
            splitPane.setOneTouchExpandable(true);
            splitPane.setLeftComponent(getFittedDataPanel());
            splitPane.setRightComponent(new JScrollPane(getParametersPanel()));
            splitPane.setDividerLocation(0.4);
            splitPane.setResizeWeight(0.4);
            splitPane.setPreferredSize(new Dimension(0, 0));
        }
        return splitPane;
    }

    private Action getAdvancedAction() {
        if (advancedAction == null) {
            advancedAction = new Action();
        }
        return advancedAction;
    }

    private Curve getAdvancedCurve() {
        if (advancedCurve == null) {
            advancedCurve = new Curve();
        }
        return advancedCurve;
    }

    private Parameters getAdvancedParameters() {
        if (advancedParameters == null) {
            advancedParameters = new Parameters(true);
        }
        return advancedParameters;
    }

    private JPanel getAdvancedPanel() {
        if (advancedPanel == null) {
            advancedPanel = new JPanel();
            advancedPanel.setName(ADVANCED_PANEL);
            advancedPanel.setLayout(new GridBagLayout());

            Insets gap = new Insets(3, 3, 3, 3);

            GridBagConstraints parametersConstraints = new GridBagConstraints();
            parametersConstraints.fill = GridBagConstraints.BOTH;
            parametersConstraints.gridx = 0;
            parametersConstraints.gridy = 0;
            parametersConstraints.weightx = 1.0D / 3.0D;
            parametersConstraints.weighty = 0.5D;
            parametersConstraints.insets = gap;

            GridBagConstraints initialConstraints = new GridBagConstraints();
            initialConstraints.fill = GridBagConstraints.BOTH;
            initialConstraints.gridx = 1;
            initialConstraints.gridy = 0;
            initialConstraints.weightx = 1.0D / 3.0D;
            initialConstraints.weighty = 0.5D;
            initialConstraints.insets = gap;

            GridBagConstraints characteristicConstraints = new GridBagConstraints();
            characteristicConstraints.fill = GridBagConstraints.BOTH;
            characteristicConstraints.gridx = 2;
            characteristicConstraints.gridy = 0;
            characteristicConstraints.weightx = 1.0D / 3.0D;
            characteristicConstraints.weighty = 0.5D;
            characteristicConstraints.insets = gap;

            GridBagConstraints qualityConstraints = new GridBagConstraints();
            qualityConstraints.fill = GridBagConstraints.BOTH;
            qualityConstraints.gridx = 0;
            qualityConstraints.gridy = 1;
            qualityConstraints.weightx = 1.0D / 3.0D;
            qualityConstraints.weighty = 0.5D;
            qualityConstraints.insets = gap;

            GridBagConstraints curveConstraints = new GridBagConstraints();
            curveConstraints.fill = GridBagConstraints.BOTH;
            curveConstraints.gridx = 1;
            curveConstraints.gridy = 1;
            curveConstraints.weightx = 1.0D / 3.0D;
            curveConstraints.weighty = 0.5D;
            curveConstraints.insets = gap;

            GridBagConstraints proxyConstraints = new GridBagConstraints();
            proxyConstraints.fill = GridBagConstraints.BOTH;
            proxyConstraints.gridx = 2;
            proxyConstraints.gridy = 1;
            proxyConstraints.weightx = 1.0D / 3.0D;
            proxyConstraints.weighty = 0.5D;
            proxyConstraints.insets = gap;

            GridBagConstraints actionConstraints = new GridBagConstraints();
            actionConstraints.fill = GridBagConstraints.HORIZONTAL;
            actionConstraints.gridx = 0;
            actionConstraints.gridy = 2;
            actionConstraints.weightx = 1;
            actionConstraints.weighty = 0;
            actionConstraints.gridwidth = GridBagConstraints.REMAINDER;
            actionConstraints.insets = gap;

            advancedPanel.add(getAdvancedParameters(), parametersConstraints);
            advancedPanel.add(getAdvancedQuality(), qualityConstraints);
            advancedPanel.add(getAdvancedCurve(), curveConstraints);

            advancedPanel.add(getAdvancedInitial(), initialConstraints);
            advancedPanel.add(getAdvancedCharacteristics(), characteristicConstraints);
            advancedPanel.add(getAdvancedProxy(), proxyConstraints);

            advancedPanel.add(getAdvancedAction(), actionConstraints);
        }
        return advancedPanel;
    }

    private Initial getAdvancedInitial() {
        if (advancedInitial == null) {
            advancedInitial = new Initial();
        }
        return advancedInitial;
    }

    private Proxy getAdvancedProxy() {
        if (advancedProxy == null) {
            advancedProxy = new Proxy();
        }
        return advancedProxy;
    }

    private Quality getQuality() {
        if (quality == null) {
            quality = new Quality();
        }
        return quality;
    }

    private Quality getAdvancedQuality() {
        if (advancedQuality == null) {
            advancedQuality = new Quality();
        }
        return advancedQuality;
    }

    private Characteristics getAdvancedCharacteristics() {
        if (advancedCharacteristics == null) {
            advancedCharacteristics = new Characteristics();
        }
        return advancedCharacteristics;
    }

    private Characteristics getCharacteristics() {
        if (characteristics == null) {
            characteristics = new Characteristics();
        }
        return characteristics;
    }

    @Override
    public synchronized void setModel(String model) {
        super.setModel(model);
        refreshChildrenConfiguration();
    }

    @Override
    protected void clearGUI() {
        cleanStatusModel();
        // cleanWidget(getFittedData());
        chartBox.disconnectWidgetFromAll(getFittedData());
        cleanWidget(getFunctionEquationViewer());
        cleanWidget(getNbDataViewer());
    }

    @Override
    protected void onConnectionError() {
        // not managed
    }

    @Override
    protected void refreshGUI() {
        if (model != null && !model.isEmpty()) {
            // Device name and status
            setStatusModel();

            // Keys construction
            String[] xTab = new String[] { getModel() + EXPERIMENTAL_DATA_X, getModel() + FITTED_DATA_X };
            String[] yTab = new String[] { getModel() + EXPERIMENTAL_DATA_Y, getModel() + FITTED_DATA_Y };

            List<IKey> listKey = generateDualAttributeKey(xTab, yTab);

            // Clear all curve
            cleanWidget(getFittedData());
            for (IKey key : listKey) {
                chartBox.connectWidget(getFittedData(), key);
                // setWidgetModel(getFittedData(), chartBox, key);
            }

            setWidgetModel(getNbDataViewer(), stringBox, generateAttributeKey(NB_DATA_ATTRIBUTE));
            setWidgetModel(getFunctionEquationViewer(), stringBox, generateAttributeKey(FONCTION_ATTRIBUTE));

            getInfoPanel().revalidate();
            getFittedDataPanel().revalidate();
        }
    }

    protected void refreshConfiguration(Component... components) {
        if (components != null) {
            for (Component comp : components) {
                if (comp instanceof AbstractTangoBox) {
                    AbstractTangoBox bean = (AbstractTangoBox) comp;
                    bean.setModel(getModel());
                    bean.revalidate();
                } else if (comp instanceof Container) {
                    refreshConfiguration(((Container) comp).getComponents());
                }
            }
        }
    }

    private void refreshChildrenConfiguration() {
        refreshConfiguration(getComponents());
    }

    private JPanel getMainPanel() {
        if (mainPanel == null) {
            mainPanel = new JPanel();
            mainPanelLayout = new CardLayout();
            mainPanel.setLayout(mainPanelLayout);
            mainPanel.add(getSplitPane(), getSplitPane().getName());
            mainPanel.add(new JScrollPane(getAdvancedPanel()), getAdvancedPanel().getName());
            mainPanelLayout.show(mainPanel, getSplitPane().getName());
        }
        return mainPanel;
    }

    private void switchVisibility() {
        if (advancedModeCheckBox.isSelected()) {
            mainPanelLayout.show(mainPanel, getAdvancedPanel().getName());
            getAdvancedPanel().revalidate();
            getAdvancedPanel().repaint();
        } else {
            mainPanelLayout.show(mainPanel, getSplitPane().getName());
            getSplitPane().revalidate();
            getSplitPane().repaint();
        }
    }

    private JPanel getFittedDataPanel() {
        if (fittedDataPanel == null) {
            fittedDataPanel = new JPanel();

            fittedDataPanel.setLayout(new GridBagLayout());

            GridBagConstraints fittedDataConstraints = new GridBagConstraints();
            fittedDataConstraints.fill = GridBagConstraints.BOTH;
            fittedDataConstraints.gridx = 0;
            fittedDataConstraints.gridy = 0;
            fittedDataConstraints.weightx = 1;
            fittedDataConstraints.weighty = 1;

            GridBagConstraints nbDataConstraints = new GridBagConstraints();
            nbDataConstraints.fill = GridBagConstraints.HORIZONTAL;
            nbDataConstraints.gridx = 0;
            nbDataConstraints.gridy = 1;
            nbDataConstraints.weightx = 1;

            Color bg = ColorTool.getColor(getFittedData().getCometeBackground());

            JScrollPane fittedDataScrollPane = new JScrollPane(getFittedData());
            fittedDataScrollPane.setBackground(bg);
            fittedDataScrollPane.getHorizontalScrollBar().setBackground(bg);
            fittedDataScrollPane.getVerticalScrollBar().setBackground(bg);

            JScrollPane infoScrollPane = new JScrollPane(getInfoPanel());
            infoScrollPane.setBackground(bg);
            infoScrollPane.getHorizontalScrollBar().setBackground(bg);
            infoScrollPane.getVerticalScrollBar().setBackground(bg);
            infoScrollPane.setMinimumSize(new Dimension(0, infoPanel.getPreferredSize().height
                    + infoScrollPane.getHorizontalScrollBar().getPreferredSize().height + 5));

            fittedDataPanel.add(fittedDataScrollPane, fittedDataConstraints);
            fittedDataPanel.add(infoScrollPane, nbDataConstraints);
        }
        return fittedDataPanel;
    }

    private JPanel getInfoPanel() {
        if (infoPanel == null) {
            infoPanel = new JPanel();

            Color cBackground = new Color(getFittedData().getCometeBackground().getRed(),
                    getFittedData().getCometeBackground().getGreen(), getFittedData().getCometeBackground().getBlue());
            infoPanel.setBackground(cBackground);

            functionEquationLabel = new JLabel();
            functionEquationLabel.setHorizontalAlignment(JLabel.CENTER);
            functionEquationLabel.setText(EQUATION_TITLE);
            functionEquationLabel.setToolTipText(EQUATION_INFO);

            nbDataLabel = new JLabel();
            nbDataLabel.setHorizontalAlignment(JLabel.CENTER);
            nbDataLabel.setText(NBDATA_TITLE);
            nbDataLabel.setToolTipText(NBDATA_INFO);

            infoPanel.setLayout(new GridBagLayout());

            Insets margin = new Insets(2, 2, 2, 2);

            GridBagConstraints functionEquationLabelConstraints = new GridBagConstraints();
            functionEquationLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
            functionEquationLabelConstraints.gridx = 0;
            functionEquationLabelConstraints.gridy = 0;
            functionEquationLabelConstraints.insets = margin;
            GridBagConstraints functionEquationViewerConstraints = new GridBagConstraints();
            functionEquationViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            functionEquationViewerConstraints.gridx = 1;
            functionEquationViewerConstraints.gridy = 0;
            functionEquationViewerConstraints.weightx = 1;
            functionEquationViewerConstraints.gridwidth = GridBagConstraints.REMAINDER;
            functionEquationViewerConstraints.insets = margin;

            GridBagConstraints nbDataLabelConstraints = new GridBagConstraints();
            nbDataLabelConstraints.fill = GridBagConstraints.BOTH;
            nbDataLabelConstraints.gridx = 0;
            nbDataLabelConstraints.gridy = 1;
            nbDataLabelConstraints.insets = margin;
            GridBagConstraints nbDataReadConstraints = new GridBagConstraints();
            nbDataReadConstraints.fill = GridBagConstraints.HORIZONTAL;
            nbDataReadConstraints.gridx = 1;
            nbDataReadConstraints.gridy = 1;
            nbDataReadConstraints.weightx = 1;
            nbDataReadConstraints.gridwidth = GridBagConstraints.REMAINDER;
            nbDataReadConstraints.insets = margin;

            infoPanel.add(functionEquationLabel, functionEquationLabelConstraints);

            infoPanel.add(getFunctionEquationViewer(), functionEquationViewerConstraints);

            infoPanel.add(nbDataLabel, nbDataLabelConstraints);
            infoPanel.add(getNbDataViewer(), nbDataReadConstraints);
        }
        return infoPanel;
    }

    private TextField getNbDataViewer() {
        if (nbDataViewer == null) {
            nbDataViewer = generateTextField();
            nbDataViewer.setToolTipText(NBDATA_INFO);
            stringBox.setReadOnly(nbDataViewer, true);
        }
        return nbDataViewer;
    }

    private Label getFunctionEquationViewer() {
        if (functionEquationViewer == null) {
            functionEquationViewer = generateLabel();
            functionEquationViewer.setToolTipText(EQUATION_INFO);
            stringBox.setReadOnly(functionEquationViewer, true);
        }
        return functionEquationViewer;
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // not managed
    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // not managed
    }

    /**
     * Auto-generated main method to display this JPanel inside a new JFrame.
     */
    public static void main(String[] args) {
        final String start = "start", stop = "stop";
        JFrame frame = new JFrame(
                DataFitterBean.class.getSimpleName() + (args != null && args.length > 0 ? " " + args[0] : ""));
        final DataFitterBean bean = new DataFitterBean();
        String dataFitter = "ica/salsa/fit.1";
        String x1 = "ica/salsa/scan.1/actuator_1_1", x2 = "ica/salsa/scan.1/actuator_1_2";
        String y1 = "ica/salsa/scan.1/data_01", y2 = "ica/salsa/scan.1/data_02";
        if ((args != null) && (args.length > 0)) {
            dataFitter = args[0];
            if (args.length > 1) {
                x1 = args[1];
                if (args.length > 2) {
                    x2 = args[2];
                    if (args.length > 3) {
                        y1 = args[3];
                    }
                    if (args.length > 4) {
                        y2 = args[4];
                    }
                }
            }
        }
//        bean.setXProxiesChoice("test/scan/julien/actuator_1_1", "test/scan/julien/actuator_2_1");
//        bean.setYProxiesChoice("test/scan/julien/data_0_1", "test/scan/julien/data_0_2");
        bean.setXProxiesChoice(x1, x2);
        bean.setYProxiesChoice(y1, y2);
        bean.setModel(dataFitter);
        bean.start();
        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(bean, BorderLayout.CENTER);
        final JButton startButton = new JButton(start);
        startButton.addActionListener((e) -> {
            SwingUtilities.invokeLater(() -> {
                if (bean.stopped) {
                    startButton.setText(stop);
                    bean.start();
                } else {
                    startButton.setText(start);
                    bean.stop();
                }
            });
        });
        mainPanel.add(startButton, BorderLayout.SOUTH);
        frame.setTitle(Messages.getString("DataFitter.MAINFRAME_TITLE"));
        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}