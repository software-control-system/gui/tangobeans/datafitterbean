/**
 *
 */
package fr.soleil.comete.bean.datafitter;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.prefs.Preferences;

import javax.swing.JLabel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.service.ISourceListener;
import fr.soleil.comete.swing.BooleanComboBox;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.WheelSwitch;

/**
 * @author HARDION
 * 
 */
public class Initial extends AbstractTangoBox implements ISourceListener<String> {

    private static final long serialVersionUID = 4449287347003942313L;
    private CheckBox initialsParametersModeViewer = null;
    private BooleanComboBox initialsParametersModeEditor = null;
    protected final static String INITIAL_PARAM_MODE_INFO = Messages.getString("Initial.INITPARAMMODE_INFO");
    protected final static String INITIAL_PARAM_MODE_TITLE = Messages.getString("Initial.INITPARAMMODE_TITLE");
    private static final String PARAMETERS_MODE_ATTRIBUTE = "initialsParametersMode";

    private TextField initialPositionViewer = null;
    private WheelSwitch initialPositionEditor = null;
    protected final static String INITIAL_POSITION_INFO = Messages.getString("Initial.INITPOSITION_INFO");
    protected final static String INITIAL_POSITION_TITLE = Messages.getString("Initial.INITPOSITION_TITLE");
    private static final String POSITION_ATTRIBUTE = "initialPosition";

    private TextField initialWidthViewer = null;
    private WheelSwitch initialWidthEditor = null;
    protected final static String INITIAL_WIDTH_INFO = Messages.getString("Initial.INITWIDTH_INFO");
    protected final static String INITIAL_WIDTH_TITLE = Messages.getString("Initial.INITWIDTH_TITLE");
    private static final String WIDTH_ATTRIBUTE = "initialWidth";

    private TextField initialHeightViewer = null;
    private WheelSwitch initialHeightEditor = null;
    protected final static String INITIAL_HEIGHT_INFO = Messages.getString("Initial.INITHEIGHT_INFO");
    protected final static String INITIAL_HEIGHT_TITLE = Messages.getString("Initial.INITHEIGHT_TITLE");
    private static final String HEIGHT_ATTRIBUTE = "initialHeight";

    private JLabel initialBackgroundLabel = null;
    private TextField initialBackgroundViewer = null;
    private WheelSwitch initialBackgroundEditor = null;

    private JLabel initialABackgroundLabel = null;
    private TextField initialABackgroundViewer = null;
    private WheelSwitch initialABackgroundEditor = null;

    private JLabel initialBBackgroundLabel = null;
    private TextField initialBBackgroundViewer = null;
    private WheelSwitch initialBBackgroundEditor = null;

    protected final static String INITIAL_BACKG_INFO = Messages.getString("Initial.INITBG_INFO");
    protected final static String INITIAL_BACKG_TITLE = Messages.getString("Initial.INITBG_TITLE");
    protected final static String INITIAL_ABACKG_INFO = Messages.getString("Initial.INITABG_INFO");
    protected final static String INITIAL_ABACKG_TITLE = Messages.getString("Initial.INITABG_TITLE");
    protected final static String INITIAL_BBACKG_INFO = Messages.getString("Initial.INITBBG_INFO");
    protected final static String INITIAL_BBACKG_TITLE = Messages.getString("Initial.INITBBG_TITLE");

    private static final String BACKGROUND_ATTRIBUTE = "initialBackground";
    private static final String A_BACKGROUND_ATTRIBUTE = "initialBackgroundA";
    private static final String B_BACKGROUND_ATTRIBUTE = "initialBackgroundB";

    // Type of function last value
    private String lastValue;
    private static final String FUNCTION_TYPE_ATTRIBUTE = "fittingFunctionType";

    private GridBagConstraints initialBackgroundLabelConstraints = null;
    private GridBagConstraints initialBackgroundViewerConstraints = null;
    private GridBagConstraints initialBackgroundEditorConstraints = null;

    private GridBagConstraints initialABackgroundLabelConstraints = null;
    private GridBagConstraints initialABackgroundViewerConstraints = null;
    private GridBagConstraints initialABackgroundEditorConstraints = null;

    private GridBagConstraints initialBBackgroundLabelConstraints = null;
    private GridBagConstraints initialBBackgroundViewerConstraints = null;
    private GridBagConstraints initialBBackgroundEditorConstraints = null;

    private final BooleanScalarBox booleanBox;

    /**
     * This method initializes
     * 
     */
    public Initial() {
        super();
        booleanBox = (BooleanScalarBox) CometeBoxProvider.getCometeBox(BooleanScalarBox.class);
        initialize();
    }

    /**
     * This method initializes this
     * 
     */
    private void initialize() {
        lastValue = "";

        JLabel initialsParametersModeLabel = new JLabel();
        initialsParametersModeLabel.setHorizontalAlignment(JLabel.CENTER);
        initialsParametersModeLabel.setText(INITIAL_PARAM_MODE_TITLE);
        initialsParametersModeLabel.setToolTipText(INITIAL_PARAM_MODE_INFO);

        JLabel initialPositionLabel = new JLabel();
        initialPositionLabel.setHorizontalAlignment(JLabel.CENTER);
        initialPositionLabel.setText(INITIAL_POSITION_TITLE);
        initialPositionLabel.setToolTipText(INITIAL_POSITION_INFO);

        JLabel initialWidthLabel = new JLabel();
        initialWidthLabel.setHorizontalAlignment(JLabel.CENTER);
        initialWidthLabel.setText(INITIAL_WIDTH_TITLE);
        initialWidthLabel.setToolTipText(INITIAL_WIDTH_INFO);

        JLabel initialHeightLabel = new JLabel();
        initialHeightLabel.setHorizontalAlignment(JLabel.CENTER);
        initialHeightLabel.setText(INITIAL_HEIGHT_TITLE);
        initialHeightLabel.setToolTipText(INITIAL_HEIGHT_INFO);

        initialBackgroundLabel = new JLabel();
        initialBackgroundLabel.setHorizontalAlignment(JLabel.CENTER);
        initialBackgroundLabel.setText(INITIAL_BACKG_TITLE);
        initialBackgroundLabel.setToolTipText(INITIAL_BACKG_INFO);

        initialABackgroundLabel = new JLabel();
        initialABackgroundLabel.setHorizontalAlignment(JLabel.CENTER);
        initialABackgroundLabel.setText(INITIAL_ABACKG_TITLE);
        initialABackgroundLabel.setToolTipText(INITIAL_ABACKG_INFO);

        initialBBackgroundLabel = new JLabel();
        initialBBackgroundLabel.setHorizontalAlignment(JLabel.CENTER);
        initialBBackgroundLabel.setText(INITIAL_BBACKG_TITLE);
        initialBBackgroundLabel.setToolTipText(INITIAL_BBACKG_INFO);

        this.setLayout(new GridBagLayout());

        GridBagConstraints initialsParametersModeLabelConstraints = new GridBagConstraints();
        initialsParametersModeLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialsParametersModeLabelConstraints.gridx = 0;
        initialsParametersModeLabelConstraints.gridy = 0;
        GridBagConstraints initialsParametersModeViewerConstraints = new GridBagConstraints();
        initialsParametersModeViewerConstraints.fill = GridBagConstraints.BOTH;
        initialsParametersModeViewerConstraints.gridx = 1;
        initialsParametersModeViewerConstraints.gridy = 0;
        initialsParametersModeViewerConstraints.weightx = 0.9;
        GridBagConstraints initialsParametersModeEditorConstraints = new GridBagConstraints();
        initialsParametersModeEditorConstraints.fill = GridBagConstraints.BOTH;
        initialsParametersModeEditorConstraints.gridx = 2;
        initialsParametersModeEditorConstraints.gridy = 0;
        initialsParametersModeEditorConstraints.weightx = 0.1;

        GridBagConstraints initialPositionLabelConstraints = new GridBagConstraints();
        initialPositionLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialPositionLabelConstraints.gridx = 0;
        initialPositionLabelConstraints.gridy = 1;
        GridBagConstraints initialPositionViewerConstraints = new GridBagConstraints();
        initialPositionViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialPositionViewerConstraints.gridx = 1;
        initialPositionViewerConstraints.gridy = 1;
        initialPositionViewerConstraints.weightx = 0.9;
        GridBagConstraints initialPositionEditorConstraints = new GridBagConstraints();
        initialPositionEditorConstraints.fill = GridBagConstraints.BOTH;
        initialPositionEditorConstraints.gridx = 2;
        initialPositionEditorConstraints.gridy = 1;
        initialPositionEditorConstraints.weightx = 0.1;

        GridBagConstraints initialWidthLabelConstraints = new GridBagConstraints();
        initialWidthLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialWidthLabelConstraints.gridx = 0;
        initialWidthLabelConstraints.gridy = 2;
        GridBagConstraints initialWidthViewerConstraints = new GridBagConstraints();
        initialWidthViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialWidthViewerConstraints.gridx = 1;
        initialWidthViewerConstraints.gridy = 2;
        initialWidthViewerConstraints.weightx = 0.9;
        GridBagConstraints initialWidthEditorConstraints = new GridBagConstraints();
        initialWidthEditorConstraints.fill = GridBagConstraints.BOTH;
        initialWidthEditorConstraints.gridx = 2;
        initialWidthEditorConstraints.gridy = 2;
        initialWidthEditorConstraints.weightx = 0.1;

        GridBagConstraints initialHeightLabelConstraints = new GridBagConstraints();
        initialHeightLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialHeightLabelConstraints.gridx = 0;
        initialHeightLabelConstraints.gridy = 3;
        GridBagConstraints initialHeightViewerConstraints = new GridBagConstraints();
        initialHeightViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialHeightViewerConstraints.gridx = 1;
        initialHeightViewerConstraints.gridy = 3;
        initialHeightViewerConstraints.weightx = 0.9;
        GridBagConstraints initialHeightEditorConstraints = new GridBagConstraints();
        initialHeightEditorConstraints.fill = GridBagConstraints.BOTH;
        initialHeightEditorConstraints.gridx = 2;
        initialHeightEditorConstraints.gridy = 3;
        initialHeightEditorConstraints.weightx = 0.1;

        // DEFAULT BACKGROUND
        initialBackgroundLabelConstraints = new GridBagConstraints();
        initialBackgroundLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialBackgroundLabelConstraints.gridx = 0;
        initialBackgroundLabelConstraints.gridy = 4;
        initialBackgroundViewerConstraints = new GridBagConstraints();
        initialBackgroundViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialBackgroundViewerConstraints.gridx = 1;
        initialBackgroundViewerConstraints.gridy = 4;
        initialBackgroundViewerConstraints.weightx = 0.9;
        initialBackgroundEditorConstraints = new GridBagConstraints();
        initialBackgroundEditorConstraints.fill = GridBagConstraints.BOTH;
        initialBackgroundEditorConstraints.gridx = 2;
        initialBackgroundEditorConstraints.gridy = 4;
        initialBackgroundEditorConstraints.weightx = 0.1;

        // A BACKGROUND
        initialABackgroundLabelConstraints = new GridBagConstraints();
        initialABackgroundLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialABackgroundLabelConstraints.gridx = 0;
        initialABackgroundLabelConstraints.gridy = 5;
        initialABackgroundViewerConstraints = new GridBagConstraints();
        initialABackgroundViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialABackgroundViewerConstraints.gridx = 1;
        initialABackgroundViewerConstraints.gridy = 5;
        initialABackgroundViewerConstraints.weightx = 0.9;
        initialABackgroundEditorConstraints = new GridBagConstraints();
        initialABackgroundEditorConstraints.fill = GridBagConstraints.BOTH;
        initialABackgroundEditorConstraints.gridx = 2;
        initialABackgroundEditorConstraints.gridy = 5;
        initialABackgroundEditorConstraints.weightx = 0.1;

        // B BACKGROUND
        initialBBackgroundLabelConstraints = new GridBagConstraints();
        initialBBackgroundLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialBBackgroundLabelConstraints.gridx = 0;
        initialBBackgroundLabelConstraints.gridy = 6;
        initialBBackgroundViewerConstraints = new GridBagConstraints();
        initialBBackgroundViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        initialBBackgroundViewerConstraints.gridx = 1;
        initialBBackgroundViewerConstraints.gridy = 6;
        initialBBackgroundViewerConstraints.weightx = 0.9;
        initialBBackgroundEditorConstraints = new GridBagConstraints();
        initialBBackgroundEditorConstraints.fill = GridBagConstraints.BOTH;
        initialBBackgroundEditorConstraints.gridx = 2;
        initialBBackgroundEditorConstraints.gridy = 6;
        initialBBackgroundEditorConstraints.weightx = 0.1;

        this.add(initialsParametersModeLabel, initialsParametersModeLabelConstraints);
        this.add(getInitialsParametersModeViewer(), initialsParametersModeViewerConstraints);
        this.add(getInitialsParametersModeEditor(), initialsParametersModeEditorConstraints);

        this.add(initialPositionLabel, initialPositionLabelConstraints);
        this.add(getInitialPositionViewer(), initialPositionViewerConstraints);
        this.add(getInitialPositionEditor(), initialPositionEditorConstraints);

        this.add(initialWidthLabel, initialWidthLabelConstraints);
        this.add(getInitialWidthViewer(), initialWidthViewerConstraints);
        this.add(getInitialWidthEditor(), initialWidthEditorConstraints);

        this.add(initialHeightLabel, initialHeightLabelConstraints);
        this.add(getInitialHeightViewer(), initialHeightViewerConstraints);
        this.add(getInitialHeightEditor(), initialHeightEditorConstraints);

        this.add(initialBackgroundLabel, initialBackgroundLabelConstraints);
        this.add(getInitialBackgroundViewer(), initialBackgroundViewerConstraints);
        this.add(getInitialBackgroundEditor(), initialBackgroundEditorConstraints);

        this.add(initialABackgroundLabel, initialABackgroundLabelConstraints);
        this.add(getInitialABackgroundViewer(), initialABackgroundViewerConstraints);
        this.add(getInitialABackgroundEditor(), initialABackgroundEditorConstraints);

        this.add(initialBBackgroundLabel, initialBBackgroundLabelConstraints);
        this.add(getInitialBBackgroundViewer(), initialBBackgroundViewerConstraints);
        this.add(getInitialBBackgroundEditor(), initialBBackgroundEditorConstraints);

        getInitialsParametersModeEditor().setCometeFont(getInitialsParametersModeViewer().getCometeFont());
        getInitialPositionEditor().setCometeFont(getInitialPositionViewer().getCometeFont());
        getInitialWidthEditor().setCometeFont(getInitialWidthViewer().getCometeFont());
        getInitialHeightEditor().setCometeFont(getInitialHeightViewer().getCometeFont());
        getInitialBackgroundEditor().setCometeFont(getInitialBackgroundViewer().getCometeFont());

        this.setBorder(new TitledBorder(new LineBorder(DataFitterBean.BORDER_COLOR, 1),
                Messages.getString("Initial.BORDER_TITLE"), TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), DataFitterBean.TITLE_COLOR));
    }

    @Override
    protected void clearGUI() {
        cleanWidget(getInitialBackgroundEditor());
        cleanWidget(getInitialBackgroundViewer());
        cleanWidget(getInitialABackgroundEditor());
        cleanWidget(getInitialABackgroundViewer());
        cleanWidget(getInitialBBackgroundEditor());
        cleanWidget(getInitialBBackgroundViewer());
        cleanWidget(getInitialHeightEditor());
        cleanWidget(getInitialHeightViewer());
        cleanWidget(getInitialPositionEditor());
        cleanWidget(getInitialPositionViewer());
        cleanWidget(getInitialsParametersModeEditor());
        cleanWidget(getInitialsParametersModeViewer());
        cleanWidget(getInitialWidthEditor());
        cleanWidget(getInitialWidthViewer());
    }

    @Override
    protected void onConnectionError() {
        // TODO Auto-generated method stub

    }

    @Override
    protected void refreshGUI() {

        setWidgetModel(getInitialBackgroundEditor(), stringBox,
                generateWriteAttributeKey(Initial.BACKGROUND_ATTRIBUTE));
        setWidgetModel(getInitialBackgroundViewer(), stringBox, generateAttributeKey(Initial.BACKGROUND_ATTRIBUTE));

        setWidgetModel(getInitialABackgroundEditor(), stringBox,
                generateWriteAttributeKey(Initial.A_BACKGROUND_ATTRIBUTE));
        setWidgetModel(getInitialABackgroundViewer(), stringBox, generateAttributeKey(Initial.A_BACKGROUND_ATTRIBUTE));

        setWidgetModel(getInitialBBackgroundEditor(), stringBox,
                generateWriteAttributeKey(Initial.B_BACKGROUND_ATTRIBUTE));
        setWidgetModel(getInitialBBackgroundViewer(), stringBox, generateAttributeKey(Initial.B_BACKGROUND_ATTRIBUTE));

        setWidgetModel(getInitialHeightEditor(), stringBox, generateWriteAttributeKey(Initial.HEIGHT_ATTRIBUTE));
        setWidgetModel(getInitialHeightViewer(), stringBox, generateAttributeKey(Initial.HEIGHT_ATTRIBUTE));
        setWidgetModel(getInitialPositionEditor(), stringBox, generateWriteAttributeKey(Initial.POSITION_ATTRIBUTE));
        setWidgetModel(getInitialPositionViewer(), stringBox, generateAttributeKey(Initial.POSITION_ATTRIBUTE));
        setWidgetModel(getInitialsParametersModeEditor(), stringBox,
                generateWriteAttributeKey(Initial.PARAMETERS_MODE_ATTRIBUTE));
        setWidgetModel(getInitialsParametersModeViewer(), booleanBox,
                generateAttributeKey(Initial.PARAMETERS_MODE_ATTRIBUTE));
        setWidgetModel(getInitialWidthEditor(), stringBox, generateWriteAttributeKey(Initial.WIDTH_ATTRIBUTE));
        setWidgetModel(getInitialWidthViewer(), stringBox, generateAttributeKey(Initial.WIDTH_ATTRIBUTE));

        stringBox.addSourceListener(this, generateAttributeKey(Initial.FUNCTION_TYPE_ATTRIBUTE));
    }

    private WheelSwitch getInitialBackgroundEditor() {
        if (initialBackgroundEditor == null) {
            initialBackgroundEditor = generateWheelSwitch();
            initialBackgroundEditor.setCometeBackground(
                    new CometeColor(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue()));
            initialBackgroundEditor.setToolTipText(INITIAL_BACKG_INFO);
        }
        return initialBackgroundEditor;
    }

    private WheelSwitch getInitialABackgroundEditor() {
        if (initialABackgroundEditor == null) {
            initialABackgroundEditor = generateWheelSwitch();
            initialABackgroundEditor.setCometeBackground(
                    new CometeColor(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue()));
            initialABackgroundEditor.setToolTipText(INITIAL_BACKG_INFO);
        }
        return initialABackgroundEditor;
    }

    private WheelSwitch getInitialBBackgroundEditor() {
        if (initialBBackgroundEditor == null) {
            initialBBackgroundEditor = generateWheelSwitch();
            initialBBackgroundEditor.setCometeBackground(
                    new CometeColor(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue()));
            initialBBackgroundEditor.setToolTipText(INITIAL_BACKG_INFO);
        }
        return initialBBackgroundEditor;
    }

    private TextField getInitialBackgroundViewer() {
        if (initialBackgroundViewer == null) {
            initialBackgroundViewer = generateTextField();
            initialBackgroundViewer.setToolTipText(INITIAL_BACKG_INFO);
            stringBox.setReadOnly(initialBackgroundViewer, true);
        }
        return initialBackgroundViewer;
    }

    private TextField getInitialABackgroundViewer() {
        if (initialABackgroundViewer == null) {
            initialABackgroundViewer = generateTextField();
            initialABackgroundViewer.setToolTipText(INITIAL_BACKG_INFO);
            stringBox.setReadOnly(initialABackgroundViewer, true);
        }
        return initialABackgroundViewer;
    }

    private TextField getInitialBBackgroundViewer() {
        if (initialBBackgroundViewer == null) {
            initialBBackgroundViewer = generateTextField();
            initialBBackgroundViewer.setToolTipText(INITIAL_BACKG_INFO);
            stringBox.setReadOnly(initialBBackgroundViewer, true);
        }
        return initialBBackgroundViewer;
    }

    private WheelSwitch getInitialHeightEditor() {
        if (initialHeightEditor == null) {
            initialHeightEditor = generateWheelSwitch();
            initialHeightEditor.setCometeBackground(
                    new CometeColor(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue()));
            initialHeightEditor.setToolTipText(INITIAL_HEIGHT_INFO);
        }
        return initialHeightEditor;
    }

    private TextField getInitialHeightViewer() {
        if (initialHeightViewer == null) {
            initialHeightViewer = generateTextField();
            initialHeightViewer.setToolTipText(INITIAL_HEIGHT_INFO);
            stringBox.setReadOnly(initialHeightViewer, true);
        }
        return initialHeightViewer;
    }

    private BooleanComboBox getInitialsParametersModeEditor() {
        if (initialsParametersModeEditor == null) {
            initialsParametersModeEditor = generateBooleanComboBox();
            initialsParametersModeEditor.setCometeBackground(
                    new CometeColor(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue()));
            initialsParametersModeEditor.setToolTipText(INITIAL_PARAM_MODE_INFO);
        }
        return initialsParametersModeEditor;
    }

    private CheckBox getInitialsParametersModeViewer() {
        if (initialsParametersModeViewer == null) {
            initialsParametersModeViewer = generateCheckbox();
            initialsParametersModeViewer.setTrueLabel(Messages.getString("Initial.TRUE"));
            initialsParametersModeViewer.setFalseLabel(Messages.getString("Initial.FALSE"));
            initialsParametersModeViewer.setToolTipText(INITIAL_PARAM_MODE_INFO);
        }
        return initialsParametersModeViewer;
    }

    private WheelSwitch getInitialPositionEditor() {
        if (initialPositionEditor == null) {
            initialPositionEditor = generateWheelSwitch();
            initialPositionEditor.setCometeBackground(
                    new CometeColor(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue()));
            initialPositionEditor.setToolTipText(INITIAL_POSITION_INFO);
        }
        return initialPositionEditor;
    }

    private TextField getInitialPositionViewer() {
        if (initialPositionViewer == null) {
            initialPositionViewer = generateTextField();
            initialPositionViewer.setToolTipText(INITIAL_POSITION_INFO);
            stringBox.setReadOnly(initialPositionViewer, true);
        }
        return initialPositionViewer;
    }

    private WheelSwitch getInitialWidthEditor() {
        if (initialWidthEditor == null) {
            initialWidthEditor = generateWheelSwitch();
            initialWidthEditor.setCometeBackground(
                    new CometeColor(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue()));
            initialWidthEditor.setToolTipText(INITIAL_WIDTH_INFO);
        }
        return initialWidthEditor;
    }

    private TextField getInitialWidthViewer() {
        if (initialWidthViewer == null) {
            initialWidthViewer = generateTextField();
            initialWidthViewer.setToolTipText(INITIAL_WIDTH_INFO);
            stringBox.setReadOnly(initialWidthViewer, true);
        }
        return initialWidthViewer;
    }

    private void updateDynamicFields() {
        if (lastValue.trim().endsWith("b") || lastValue.equals(Parameters.MEANWITHTHESHOLD)) {
            if (initialBackgroundLabel.getParent() == null) {
                this.add(initialBackgroundLabel, initialBackgroundLabelConstraints);
                this.add(getInitialBackgroundViewer(), initialBackgroundViewerConstraints);
                this.add(getInitialBackgroundEditor(), initialBackgroundEditorConstraints);
            }
        } else {
            if (initialBackgroundLabel.getParent() != null) {
                this.remove(initialBackgroundLabel);
                this.remove(getInitialBackgroundViewer());
                this.remove(getInitialBackgroundEditor());
            }
        }
        revalidate();
        repaint();
    }

    @Override
    public void onSourceChange(String data) {
        if (data != null && !data.equals(lastValue)) {
            lastValue = data;
            updateDynamicFields();
        }
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

}