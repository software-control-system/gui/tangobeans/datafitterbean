package fr.soleil.comete.bean.datafitter;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public final class Messages {

    private static final String BUNDLE_NAME = "fr.soleil.comete.bean.datafitter.messages";
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);

    private Messages() {
    }

    public static String getString(String key) {
        String result = null;
        try {
            result = RESOURCE_BUNDLE.getString(key);
        } catch (MissingResourceException e) {
            result = '!' + key + '!';
        }
        return result;
    }

}
