/**
 *
 */
package fr.soleil.comete.bean.datafitter;

import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.prefs.Preferences;

import javax.swing.JLabel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.box.matrixbox.StringMatrixBox;
import fr.soleil.comete.box.scalarbox.BooleanScalarBox;
import fr.soleil.comete.definition.widget.util.CometeColor;
import fr.soleil.comete.service.CometeBoxProvider;
import fr.soleil.comete.swing.BooleanComboBox;
import fr.soleil.comete.swing.CheckBox;
import fr.soleil.comete.swing.StringMatrixComboBoxViewer;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.WheelSwitch;

/**
 * @author HARDION
 * 
 */
public class Parameters extends AbstractTangoBox {

    private static final long serialVersionUID = -1792071631377257439L;

    private TextField functionTypeViewer = null;
    private StringMatrixComboBoxViewer functionTypeEditor = null;
    protected final static String FUNCTION_TYPE_INFO = Messages.getString("Parameters.FUNCTION_INFO");
    protected final static String FUNCTION_TYPE_TITLE = Messages.getString("Parameters.FUNCTION_TITLE");
    private static final String FONCTION_ATTRIBUTE = "fittingFunctionType";
    private static final String FONCTIONS_LIST = "knownFittersList";

    private TextField nbIterationMaxViewer = null;
    private WheelSwitch nbIterationMaxEditor = null;
    protected final static String NB_ITER_MAX_INFO = Messages.getString("Parameters.NBITERMAX_INFO");
    protected final static String NB_ITER_MAX_TITLE = Messages.getString("Parameters.NBITERMAX_TITLE");
    private static final String MAX_ITERATION_ATTRIBUTE = "nbIterationMax";

    private TextField epsilonViewer = null;
    private WheelSwitch epsilonEditor = null;
    protected final static String EPSILON_INFO = Messages.getString("Parameters.EPSILON_INFO");
    protected final static String EPSILON_TITLE = Messages.getString("Parameters.EPSILON_TITLE");
    private static final String EPSILON_ATTRIBUTE = "epsilon";

    private CheckBox useSigmaViewer = null;
    private BooleanComboBox useSigmaEditor = null;
    protected final static String USE_SIGMA_INFO = Messages.getString("Parameters.USESIGMA_INFO");
    protected final static String USE_SIGMA_TITLE = Messages.getString("Parameters.USESIGMA_TITLE");
    private static final String USE_SIGMA_ATTRIBUTE = "useSigma";

    private CheckBox useScaledViewer = null;
    private BooleanComboBox useScaledEditor = null;
    protected final static String USE_SCALED_INFO = Messages.getString("Parameters.USESCALED_INFO");
    protected final static String USE_SCALED_TITLE = Messages.getString("Parameters.USESCALED_TITLE");
    private static final String FIT_MODE_ATTRIBUTE = "fitMode";

    private CheckBox reverseYViewer = null;
    private BooleanComboBox reverseYEditor = null;
    protected final static String REVERSE_Y_INFO = Messages.getString("Parameters.REVERSE_Y_INFO");
    protected final static String REVERSE_Y__TITLE = Messages.getString("Parameters.REVERSE_Y_TITLE");
    private static final String REVERSE_Y_ATTRIBUTE = "reverseY";

    private CheckBox fitModeCheckBoxViewer = null;
    private BooleanComboBox fitModeEditor = null;
    protected final static String FIT_MODE_INFO = Messages.getString("Parameters.FITMODE_INFO");
    protected final static String FIT_MODE_TITLE = Messages.getString("Parameters.FITMODE_TITLE");
    private static final String USE_SCALED_ATTRIBUTE = "useScaled";

    private TextField searchStoppingMethodViewer = null;
    private WheelSwitch searchStoppingMethodEditor = null;
    protected final static String METHOD_INFO = Messages.getString("Parameters.STOPPINGMETHOD_INFO");
    protected final static String METHOD_TITLE = Messages.getString("Parameters.STOPPINGMETHOD_TITLE");
    private static final String METHODE_ATTRIBUTE = "searchStoppingMethod";

    protected final static String GAUSSIAN = "gaussian";
    protected final static String GAUSSIANB = "gaussianb";
    protected final static String LORENTZIAN = "lorentzian";
    protected final static String LORENTZIANB = "lorentzianb";
    protected final static String SIGMOID = "sigmoid";
    protected final static String SIGMOIDB = "sigmoidb";
    protected final static String MEANWITHTHESHOLD = "meanwiththreshold";

    protected boolean expert = false;

    private final BooleanScalarBox booleanBox;

    // Editors preferred size management
    protected int maxIterWidth = 0, epsilonWidth = 0, searchWidth = 0;

    /**
     * This method initializes
     * 
     */
    public Parameters() {
        this(false);
    }

    /**
     * This method initializes and sets whether expert mode is activated
     * 
     */
    public Parameters(boolean expert) {
        super();
        this.expert = expert;
        booleanBox = (BooleanScalarBox) CometeBoxProvider.getCometeBox(BooleanScalarBox.class);
        initialize(expert);
    }

    /**
     * This method initializes this
     * 
     */
    private void initialize(boolean expert) {

        JLabel functionTypeLabel = new JLabel();
        functionTypeLabel.setHorizontalAlignment(JLabel.CENTER);
        functionTypeLabel.setText(FUNCTION_TYPE_TITLE);
        functionTypeLabel.setToolTipText(FUNCTION_TYPE_INFO);

        JLabel nbIterationMaxLabel = new JLabel();
        nbIterationMaxLabel.setHorizontalAlignment(JLabel.CENTER);
        nbIterationMaxLabel.setText(NB_ITER_MAX_TITLE);
        nbIterationMaxLabel.setToolTipText(NB_ITER_MAX_INFO);

        JLabel epsilonLabel = new JLabel();
        epsilonLabel.setHorizontalAlignment(JLabel.CENTER);
        epsilonLabel.setText(EPSILON_TITLE);
        epsilonLabel.setToolTipText(EPSILON_INFO);

        JLabel fitModeLabel = new JLabel();
        fitModeLabel.setHorizontalAlignment(JLabel.CENTER);
        fitModeLabel.setText(FIT_MODE_TITLE);
        fitModeLabel.setToolTipText(FIT_MODE_INFO);

        JLabel reverseYLabel = new JLabel();
        reverseYLabel.setHorizontalAlignment(JLabel.CENTER);
        reverseYLabel.setText(REVERSE_Y__TITLE);
        reverseYLabel.setToolTipText(REVERSE_Y_INFO);

        JLabel searchStoppingMethodLabel = new JLabel();
        searchStoppingMethodLabel.setHorizontalAlignment(JLabel.CENTER);
        searchStoppingMethodLabel.setText(METHOD_TITLE);
        searchStoppingMethodLabel.setToolTipText(METHOD_INFO);

        JLabel useScaledLabel = new JLabel();
        useScaledLabel.setHorizontalAlignment(JLabel.CENTER);
        useScaledLabel.setText(USE_SCALED_TITLE);
        useScaledLabel.setToolTipText(USE_SCALED_INFO);

        JLabel useSigmaLabel = new JLabel();
        useSigmaLabel.setHorizontalAlignment(JLabel.CENTER);
        useSigmaLabel.setText(USE_SIGMA_TITLE);
        useSigmaLabel.setToolTipText(USE_SIGMA_INFO);

        this.setLayout(new GridBagLayout());

        GridBagConstraints fittingFunctionTypeLabelConstraints = new GridBagConstraints();
        fittingFunctionTypeLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        fittingFunctionTypeLabelConstraints.gridx = 0;
        fittingFunctionTypeLabelConstraints.gridy = 0;
        GridBagConstraints fittingFunctionTypeViewerConstraints = new GridBagConstraints();
        fittingFunctionTypeViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        fittingFunctionTypeViewerConstraints.gridx = 1;
        fittingFunctionTypeViewerConstraints.gridy = 0;
        fittingFunctionTypeViewerConstraints.weightx = 0.9;
        GridBagConstraints fittingFunctionTypeEditorConstraints = new GridBagConstraints();
        fittingFunctionTypeEditorConstraints.fill = GridBagConstraints.BOTH;
        fittingFunctionTypeEditorConstraints.gridx = 2;
        fittingFunctionTypeEditorConstraints.gridy = 0;
        fittingFunctionTypeEditorConstraints.weightx = 0.1;

        GridBagConstraints nbIterationMaxLabelConstraints = new GridBagConstraints();
        nbIterationMaxLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        nbIterationMaxLabelConstraints.gridx = 0;
        nbIterationMaxLabelConstraints.gridy = 1;
        GridBagConstraints nbIterationMaxViewerConstraints = new GridBagConstraints();
        nbIterationMaxViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        nbIterationMaxViewerConstraints.gridx = 1;
        nbIterationMaxViewerConstraints.gridy = 1;
        nbIterationMaxViewerConstraints.weightx = 0.9;
        GridBagConstraints nbIterationMaxEditorConstraints = new GridBagConstraints();
        nbIterationMaxEditorConstraints.fill = GridBagConstraints.BOTH;
        nbIterationMaxEditorConstraints.gridx = 2;
        nbIterationMaxEditorConstraints.gridy = 1;
        nbIterationMaxEditorConstraints.weightx = 0.1;

        GridBagConstraints epsilonLabelConstraints = new GridBagConstraints();
        epsilonLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        epsilonLabelConstraints.gridx = 0;
        epsilonLabelConstraints.gridy = 2;
        GridBagConstraints epsilonViewerConstraints = new GridBagConstraints();
        epsilonViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        epsilonViewerConstraints.gridx = 1;
        epsilonViewerConstraints.gridy = 2;
        epsilonViewerConstraints.weightx = 0.9;
        GridBagConstraints epsilonEditorConstraints = new GridBagConstraints();
        epsilonEditorConstraints.fill = GridBagConstraints.BOTH;
        epsilonEditorConstraints.gridx = 2;
        epsilonEditorConstraints.gridy = 2;
        epsilonEditorConstraints.weightx = 0.1;

        GridBagConstraints useSigmaLabelConstraints = new GridBagConstraints();
        useSigmaLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        useSigmaLabelConstraints.gridx = 0;
        useSigmaLabelConstraints.gridy = 3;
        GridBagConstraints useSigmaViewerConstraints = new GridBagConstraints();
        useSigmaViewerConstraints.fill = GridBagConstraints.BOTH;
        useSigmaViewerConstraints.gridx = 1;
        useSigmaViewerConstraints.gridy = 3;
        useSigmaViewerConstraints.weightx = 0.9;
        GridBagConstraints useSigmaEditorConstraints = new GridBagConstraints();
        useSigmaEditorConstraints.fill = GridBagConstraints.HORIZONTAL;
        useSigmaEditorConstraints.gridx = 2;
        useSigmaEditorConstraints.gridy = 3;
        useSigmaEditorConstraints.weightx = 0.1;

        GridBagConstraints fitModeLabelConstraints = new GridBagConstraints();
        fitModeLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        fitModeLabelConstraints.gridx = 0;
        fitModeLabelConstraints.gridy = 4;
        GridBagConstraints fitModeCheckBoxViewerConstraints = new GridBagConstraints();
        fitModeCheckBoxViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        fitModeCheckBoxViewerConstraints.gridx = 1;
        fitModeCheckBoxViewerConstraints.gridy = 4;
        fitModeCheckBoxViewerConstraints.weightx = 0.9;
        GridBagConstraints fitModeEditorConstraints = new GridBagConstraints();
        fitModeEditorConstraints.fill = GridBagConstraints.HORIZONTAL;
        fitModeEditorConstraints.gridx = 2;
        fitModeEditorConstraints.gridy = 4;
        fitModeEditorConstraints.weightx = 0.1;

        GridBagConstraints reverseYLabelConstraints = new GridBagConstraints();
        reverseYLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        reverseYLabelConstraints.gridx = 0;
        reverseYLabelConstraints.gridy = 5;

        GridBagConstraints reverseYViewerConstraints = new GridBagConstraints();
        reverseYViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        reverseYViewerConstraints.gridx = 1;
        reverseYViewerConstraints.gridy = 5;
        reverseYViewerConstraints.weightx = 0.9;

        GridBagConstraints reverseYEditorConstraints = new GridBagConstraints();
        reverseYEditorConstraints.fill = GridBagConstraints.BOTH;
        reverseYEditorConstraints.gridx = 2;
        reverseYEditorConstraints.gridy = 5;
        reverseYEditorConstraints.weightx = 0.1;

        this.add(functionTypeLabel, fittingFunctionTypeLabelConstraints);
        this.add(getFittingFunctionTypeViewer(), fittingFunctionTypeViewerConstraints);
        this.add(getFittingFunctionTypeEditor(), fittingFunctionTypeEditorConstraints);

        this.add(nbIterationMaxLabel, nbIterationMaxLabelConstraints);
        this.add(getNbIterationMaxViewer(), nbIterationMaxViewerConstraints);
        this.add(getNbIterationMaxEditor(), nbIterationMaxEditorConstraints);

        this.add(epsilonLabel, epsilonLabelConstraints);
        this.add(getEpsilonViewer(), epsilonViewerConstraints);
        this.add(getEpsilonEditor(), epsilonEditorConstraints);

        this.add(useSigmaLabel, useSigmaLabelConstraints);
        this.add(getUseSigmaViewer(), useSigmaViewerConstraints);
        this.add(getUseSigmaEditor(), useSigmaEditorConstraints);

        this.add(fitModeLabel, fitModeLabelConstraints);
        this.add(getFitModeCheckBoxViewer(), fitModeCheckBoxViewerConstraints);
        this.add(getFitModeEditor(), fitModeEditorConstraints);

        this.add(reverseYLabel, reverseYLabelConstraints);
        this.add(getReverseYViewer(), reverseYViewerConstraints);
        this.add(getReverseYEditor(), reverseYEditorConstraints);

        getNbIterationMaxEditor().setCometeFont(getNbIterationMaxViewer().getCometeFont());
        getEpsilonEditor().setCometeFont(getEpsilonViewer().getCometeFont());

        if (expert) {

            GridBagConstraints useScaledLabelConstraints = new GridBagConstraints();
            useScaledLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
            useScaledLabelConstraints.gridx = 0;
            useScaledLabelConstraints.gridy = 7;
            GridBagConstraints useScaledViewerConstraints = new GridBagConstraints();
            useScaledViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            useScaledViewerConstraints.gridx = 1;
            useScaledViewerConstraints.gridy = 7;
            useScaledViewerConstraints.weightx = 0.9;
            GridBagConstraints useScaledEditorConstraints = new GridBagConstraints();
            useScaledEditorConstraints.fill = GridBagConstraints.BOTH;
            useScaledEditorConstraints.gridx = 2;
            useScaledEditorConstraints.gridy = 7;
            useScaledEditorConstraints.weightx = 0.1;

            GridBagConstraints searchStoppingMethodLabelConstraints = new GridBagConstraints();
            searchStoppingMethodLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
            searchStoppingMethodLabelConstraints.gridx = 0;
            searchStoppingMethodLabelConstraints.gridy = 8;
            GridBagConstraints searchStoppingMethodViewerConstraints = new GridBagConstraints();
            searchStoppingMethodViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
            searchStoppingMethodViewerConstraints.gridx = 1;
            searchStoppingMethodViewerConstraints.gridy = 8;
            searchStoppingMethodViewerConstraints.weightx = 0.9;

            GridBagConstraints searchStoppingMethodEditorConstraints = new GridBagConstraints();
            searchStoppingMethodEditorConstraints.fill = GridBagConstraints.BOTH;
            searchStoppingMethodEditorConstraints.gridx = 2;
            searchStoppingMethodEditorConstraints.gridy = 8;
            searchStoppingMethodEditorConstraints.weightx = 0.1;

            this.add(useScaledLabel, useScaledLabelConstraints);
            this.add(getUseScaledViewer(), useScaledViewerConstraints);
            this.add(getUseScaledEditor(), useScaledEditorConstraints);

            this.add(searchStoppingMethodLabel, searchStoppingMethodLabelConstraints);
            this.add(getSearchStoppingMethodViewer(), searchStoppingMethodViewerConstraints);
            this.add(getSearchStoppingMethodEditor(), searchStoppingMethodEditorConstraints);

            getSearchStoppingMethodEditor().setCometeFont(getSearchStoppingMethodViewer().getCometeFont());
            getUseScaledEditor().setCometeFont(getUseScaledViewer().getCometeFont());

        }

        this.setBorder(new TitledBorder(new LineBorder(DataFitterBean.BORDER_COLOR, 1),
                Messages.getString("Parameters.BORDER_TITLE"), TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), DataFitterBean.TITLE_COLOR));
    }

    @Override
    protected void clearGUI() {
        cleanWidget(getEpsilonEditor());
        cleanWidget(getEpsilonViewer());
        cleanWidget(getFitModeCheckBoxViewer());
        cleanWidget(getFitModeEditor());
        cleanWidget(getFittingFunctionTypeEditor());
        cleanWidget(getFittingFunctionTypeViewer());
        cleanWidget(getNbIterationMaxEditor());
        cleanWidget(getNbIterationMaxViewer());
        cleanWidget(getSearchStoppingMethodEditor());
        cleanWidget(getSearchStoppingMethodViewer());
        cleanWidget(getUseScaledEditor());
        cleanWidget(getUseScaledViewer());
        cleanWidget(getReverseYEditor());
        cleanWidget(getReverseYViewer());
        cleanWidget(getUseSigmaEditor());
        cleanWidget(getUseSigmaViewer());
    }

    @Override
    protected void onConnectionError() {
        DataFitterBean.LOGGER.info("Parameters component, Connection error.");

    }

    @Override
    protected void refreshGUI() {

        setWidgetModel(getFittingFunctionTypeViewer(), stringBox, generateAttributeKey(Parameters.FONCTION_ATTRIBUTE));
        setWidgetModel(getFittingFunctionTypeEditor(), new StringMatrixBox(),
                generateAttributeKey(Parameters.FONCTIONS_LIST));

        setWidgetModel(getNbIterationMaxViewer(), stringBox, generateAttributeKey(Parameters.MAX_ITERATION_ATTRIBUTE));
        setWidgetModel(getNbIterationMaxEditor(), stringBox,
                generateWriteAttributeKey(Parameters.MAX_ITERATION_ATTRIBUTE));

        setWidgetModel(getEpsilonViewer(), stringBox, generateAttributeKey(Parameters.EPSILON_ATTRIBUTE));
        setWidgetModel(getEpsilonEditor(), stringBox, generateWriteAttributeKey(Parameters.EPSILON_ATTRIBUTE));

        setWidgetModel(getUseSigmaViewer(), booleanBox, generateAttributeKey(Parameters.USE_SIGMA_ATTRIBUTE));
        setWidgetModel(getUseSigmaEditor(), booleanBox, generateWriteAttributeKey(Parameters.USE_SIGMA_ATTRIBUTE));

        setWidgetModel(getFitModeCheckBoxViewer(), booleanBox, generateAttributeKey(Parameters.FIT_MODE_ATTRIBUTE));
        setWidgetModel(getFitModeEditor(), booleanBox, generateWriteAttributeKey(Parameters.FIT_MODE_ATTRIBUTE));

        setWidgetModel(getUseScaledViewer(), booleanBox, generateAttributeKey(Parameters.USE_SCALED_ATTRIBUTE));
        setWidgetModel(getUseScaledEditor(), stringBox, generateWriteAttributeKey(Parameters.USE_SCALED_ATTRIBUTE));

        setWidgetModel(getReverseYViewer(), booleanBox, generateAttributeKey(Parameters.REVERSE_Y_ATTRIBUTE));
        setWidgetModel(getReverseYEditor(), booleanBox, generateWriteAttributeKey(Parameters.REVERSE_Y_ATTRIBUTE));

        setWidgetModel(getSearchStoppingMethodViewer(), stringBox, generateAttributeKey(Parameters.METHODE_ATTRIBUTE));
        setWidgetModel(getSearchStoppingMethodEditor(), stringBox,
                generateWriteAttributeKey(Parameters.METHODE_ATTRIBUTE));

    }

    private WheelSwitch getEpsilonEditor() {
        if (epsilonEditor == null) {
            epsilonEditor = generateWheelSwitch();
            epsilonEditor.setToolTipText(EPSILON_INFO);
            epsilonEditor.setCometeBackground(
                    new CometeColor(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue()));
        }
        return epsilonEditor;
    }

    private TextField getEpsilonViewer() {
        if (epsilonViewer == null) {
            epsilonViewer = generateTextField();
            epsilonViewer.setToolTipText(EPSILON_INFO);
            stringBox.setReadOnly(epsilonViewer, true);
        }
        return epsilonViewer;
    }

    private StringMatrixComboBoxViewer getFittingFunctionTypeEditor() {
        if (functionTypeEditor == null) {
            functionTypeEditor = new StringMatrixComboBoxViewer();
            functionTypeEditor.setToolTipText(FUNCTION_TYPE_INFO);
            functionTypeEditor.setLinkPopupVisibilityWithEditable(false);
            functionTypeEditor.addItemListener(new ItemListener() {

                @Override
                public void itemStateChanged(ItemEvent e) {
                    if (functionTypeViewer != null) {
                        if (e.getItem() != null) {
                            functionTypeViewer.setText(e.getItem().toString());
                            functionTypeViewer.actionPerformed(null);
                        }
                    }
                }
            });
//            functionTypeEditor.setValueList(Parameters.GAUSSIAN, Parameters.GAUSSIANB,
//                    Parameters.LORENTZIAN, Parameters.LORENTZIANB, Parameters.SIGMOID,
//                    Parameters.SIGMOIDB, Parameters.MEANWITHTHESHOLD);
        }
        return functionTypeEditor;
    }

    private TextField getFittingFunctionTypeViewer() {
        if (functionTypeViewer == null) {
            functionTypeViewer = generateTextField();
            functionTypeViewer.setToolTipText(FUNCTION_TYPE_INFO);
        }
        return functionTypeViewer;
    }

    private WheelSwitch getNbIterationMaxEditor() {
        if (nbIterationMaxEditor == null) {
            nbIterationMaxEditor = generateWheelSwitch();
            nbIterationMaxEditor.setCometeBackground(
                    new CometeColor(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue()));
            nbIterationMaxEditor.setToolTipText(NB_ITER_MAX_INFO);
        }
        return nbIterationMaxEditor;
    }

    private TextField getNbIterationMaxViewer() {
        if (nbIterationMaxViewer == null) {
            nbIterationMaxViewer = generateTextField();
            nbIterationMaxViewer.setToolTipText(NB_ITER_MAX_INFO);
            stringBox.setReadOnly(nbIterationMaxViewer, true);
        }
        return nbIterationMaxViewer;
    }

    private WheelSwitch getSearchStoppingMethodEditor() {
        if (searchStoppingMethodEditor == null) {
            searchStoppingMethodEditor = generateWheelSwitch();
            searchStoppingMethodEditor.setCometeBackground(
                    new CometeColor(getBackground().getRed(), getBackground().getGreen(), getBackground().getBlue()));
            searchStoppingMethodEditor.setToolTipText(METHOD_INFO);
        }
        return searchStoppingMethodEditor;
    }

    private TextField getSearchStoppingMethodViewer() {
        if (searchStoppingMethodViewer == null) {
            searchStoppingMethodViewer = generateTextField();
            searchStoppingMethodViewer.setToolTipText(METHOD_INFO);
            stringBox.setReadOnly(searchStoppingMethodViewer, true);
        }
        return searchStoppingMethodViewer;
    }

    private CheckBox getUseSigmaViewer() {
        if (useSigmaViewer == null) {
            useSigmaViewer = generateCheckbox();
            useSigmaViewer.setTrueLabel(Messages.getString("Parameters.TRUE"));
            useSigmaViewer.setFalseLabel(Messages.getString("Parameters.FALSE"));
            useSigmaViewer.setToolTipText(USE_SIGMA_INFO);
        }
        return useSigmaViewer;
    }

    private BooleanComboBox getUseSigmaEditor() {
        if (useSigmaEditor == null) {
            useSigmaEditor = generateBooleanComboBox();
            useSigmaEditor.setToolTipText(USE_SIGMA_INFO);
        }
        return useSigmaEditor;
    }

    private BooleanComboBox getUseScaledEditor() {
        if (useScaledEditor == null) {
            useScaledEditor = generateBooleanComboBox();
            useScaledEditor.setToolTipText(USE_SCALED_INFO);
        }
        return useScaledEditor;
    }

    private BooleanComboBox getReverseYEditor() {
        if (reverseYEditor == null) {
            reverseYEditor = generateBooleanComboBox();
            reverseYEditor.setTrueLabel(Messages.getString("Parameters.TRUE"));
            reverseYEditor.setFalseLabel(Messages.getString("Parameters.FALSE"));
            reverseYEditor.setToolTipText(REVERSE_Y_INFO);
        }
        return reverseYEditor;
    }

    private CheckBox getUseScaledViewer() {
        if (useScaledViewer == null) {
            useScaledViewer = generateCheckbox();
            useScaledViewer.setTrueLabel(Messages.getString("Parameters.TRUE"));
            useScaledViewer.setFalseLabel(Messages.getString("Parameters.FALSE"));
            useScaledViewer.setToolTipText(USE_SCALED_INFO);
        }
        return useScaledViewer;
    }

    private CheckBox getReverseYViewer() {
        if (reverseYViewer == null) {
            reverseYViewer = generateCheckbox();
            reverseYViewer.setTrueLabel(Messages.getString("Parameters.TRUE"));
            reverseYViewer.setFalseLabel(Messages.getString("Parameters.FALSE"));
            reverseYViewer.setToolTipText(REVERSE_Y_INFO);
        }
        return reverseYViewer;
    }

    public boolean isExpert() {
        return expert;
    }

    /**
     * This method initializes fitModeCheckBoxViewer
     * 
     * @return fr.esrf.tangoatk.widget.attribute.BooleanScalarCheckBoxViewer
     */
    private CheckBox getFitModeCheckBoxViewer() {
        if (fitModeCheckBoxViewer == null) {
            fitModeCheckBoxViewer = generateCheckbox();
            fitModeCheckBoxViewer.setTrueLabel(Messages.getString("Parameters.TRUE"));
            fitModeCheckBoxViewer.setFalseLabel(Messages.getString("Parameters.FALSE"));
            fitModeCheckBoxViewer.setToolTipText(FIT_MODE_INFO);
        }
        return fitModeCheckBoxViewer;
    }

    private BooleanComboBox getFitModeEditor() {
        if (fitModeEditor == null) {
            fitModeEditor = new BooleanComboBox();
            fitModeEditor.setToolTipText(FIT_MODE_INFO);
        }
        return fitModeEditor;
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

}