/**
 *
 */
package fr.soleil.comete.bean.datafitter;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.util.prefs.Preferences;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.swing.ComboBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TextField;
import fr.soleil.comete.swing.border.CometeTitledBorder;
import fr.soleil.comete.swing.border.util.CometeTitledBorderToolTipManager;

/**
 * @author HARDION
 * 
 */
public class Proxy extends AbstractTangoBox {

    private static final long serialVersionUID = 1537932360186754054L;

    protected static final Font TITLE_FONT = new Font(Font.DIALOG, Font.BOLD, 12);
    protected final static String X_INFO = Messages.getString("Proxy.X_INFO");
    protected final static String X_TITLE = Messages.getString("Proxy.X_TITLE");
    protected final static String SIGMA_INFO = Messages.getString("Proxy.SIGMA_INFO");
    protected final static String SIGMA_TITLE = Messages.getString("Proxy.SIGMA_TITLE");
    protected final static String NAME_X_ATTRIBUTE = "deviceAttributeNameX";
    protected final static String NAME_SIGMA_ATTRIBUTE = "deviceAttributeNameSigma";

    private JLabel xLabel;
    private TextField xWrite;
    private Label xRead;

    private JLabel yLabel;
    private TextField yWrite;
    private Label yRead;
    protected final static String Y_INFO = Messages.getString("Proxy.Y_INFO");
    protected final static String Y_TITLE = Messages.getString("Proxy.Y_TITLE");
    private final static String NAME_Y_ATTRIBUTE = "deviceAttributeNameY";

    private JLabel sigmaLabel;
    private TextField sigmaWrite;
    private Label sigmaRead;

    private String[] xProxiesChoice;
    private String[] yProxiesChoice;

    private ComboBox yProxiesCombo;
    private ComboBox xProxiesCombo;

    /**
     * This method initializes
     * 
     */
    public Proxy() {
        super();
        initialize();
    }

    public String[] getXProxiesChoice() {
        return xProxiesChoice;
    }

    public void setXProxiesChoice(String... xProxiesChoice) {
        this.xProxiesChoice = xProxiesChoice;
        if (xProxiesChoice != null) {
            getXWriteCombo().setVisible(true);
            getXWriteCombo().setValueList((Object[]) xProxiesChoice);
            getXWrite().setVisible(false);
        } else {
            getXWriteCombo().setVisible(false);
            getXWrite().setVisible(true);
        }
    }

    public String[] getYProxiesChoice() {
        return yProxiesChoice;
    }

    public void setYProxiesChoice(String... yProxiesChoice) {
        this.yProxiesChoice = yProxiesChoice;
        if (yProxiesChoice != null) {
            getYWriteCombo().setVisible(true);
            getYWriteCombo().setValueList((Object[]) yProxiesChoice);
            getYWrite().setVisible(false);
        } else {
            getYWriteCombo().setVisible(false);
            getYWrite().setVisible(true);
        }
    }

    /**
     * This method initializes this
     * 
     */
    private void initialize() {
        setLayout(new GridBagLayout());
        addPropertyChangeListener(new CometeTitledBorderToolTipManager());

        sigmaLabel = new JLabel();
        sigmaLabel.setHorizontalAlignment(JLabel.CENTER);
        sigmaLabel.setText(SIGMA_TITLE);
        sigmaLabel.setToolTipText(SIGMA_INFO);

        yLabel = new JLabel();
        yLabel.setHorizontalAlignment(JLabel.CENTER);
        yLabel.setText(Y_TITLE);
        yLabel.setToolTipText(Y_INFO);
        xLabel = new JLabel();
        xLabel.setHorizontalAlignment(JLabel.CENTER);
        xLabel.setText(X_TITLE);
        xLabel.setToolTipText(X_INFO);

        Insets insets = new Insets(0, 0, 10, 0);
        GridBagConstraints xLabelConstraints = new GridBagConstraints();
        xLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        xLabelConstraints.gridx = 0;
        xLabelConstraints.gridy = 0;
        xLabelConstraints.gridheight = 3;
        GridBagConstraints xReadConstraints = new GridBagConstraints();
        xReadConstraints.fill = GridBagConstraints.HORIZONTAL;
        xReadConstraints.gridx = 1;
        xReadConstraints.gridy = 0;
        GridBagConstraints xWriteConstraints = new GridBagConstraints();
        xWriteConstraints.fill = GridBagConstraints.HORIZONTAL;
        xWriteConstraints.gridx = 1;
        xWriteConstraints.gridy = 1;
        xWriteConstraints.insets = insets;
        GridBagConstraints xWriteComboConstraints = new GridBagConstraints();
        xWriteComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        xWriteComboConstraints.gridx = 1;
        xWriteComboConstraints.gridy = 2;
        xWriteComboConstraints.insets = insets;

        GridBagConstraints yLabelConstraints = new GridBagConstraints();
        yLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        yLabelConstraints.gridx = 0;
        yLabelConstraints.gridy = 3;
        yLabelConstraints.gridheight = 3;
        yLabelConstraints.insets = insets;
        GridBagConstraints yReadConstraints = new GridBagConstraints();
        yReadConstraints.fill = GridBagConstraints.BOTH;
        yReadConstraints.gridx = 1;
        yReadConstraints.gridy = 3;
        GridBagConstraints yWriteConstraints = new GridBagConstraints();
        yWriteConstraints.fill = GridBagConstraints.HORIZONTAL;
        yWriteConstraints.gridx = 1;
        yWriteConstraints.gridy = 4;
        yWriteConstraints.insets = insets;
        GridBagConstraints yWriteComboConstraints = new GridBagConstraints();
        yWriteComboConstraints.fill = GridBagConstraints.HORIZONTAL;
        yWriteComboConstraints.gridx = 1;
        yWriteComboConstraints.gridy = 5;
        yWriteComboConstraints.insets = insets;

        GridBagConstraints sigmaLabelConstraints = new GridBagConstraints();
        sigmaLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        sigmaLabelConstraints.gridx = 0;
        sigmaLabelConstraints.gridy = 6;
        sigmaLabelConstraints.gridheight = 2;
        sigmaLabelConstraints.insets = insets;
        GridBagConstraints sigmaReadConstraints = new GridBagConstraints();
        sigmaReadConstraints.fill = GridBagConstraints.BOTH;
        sigmaReadConstraints.gridx = 1;
        sigmaReadConstraints.gridy = 7;
        GridBagConstraints sigmaWriteConstraints = new GridBagConstraints();
        sigmaWriteConstraints.fill = GridBagConstraints.HORIZONTAL;
        sigmaWriteConstraints.gridx = 1;
        sigmaWriteConstraints.gridy = 8;
        sigmaWriteConstraints.insets = insets;

        setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        add(xLabel, xLabelConstraints);
        add(getXRead(), xReadConstraints);
        add(getXWrite(), xWriteConstraints);
        add(getXWriteCombo(), xWriteComboConstraints);
        add(yLabel, yLabelConstraints);
        add(getYRead(), yReadConstraints);
        add(getYWrite(), yWriteConstraints);
        add(getYWriteCombo(), yWriteComboConstraints);
        add(sigmaLabel, sigmaLabelConstraints);
        add(getSigmaRead(), sigmaReadConstraints);
        add(getSigmaWrite(), sigmaWriteConstraints);
        CometeTitledBorder border = new CometeTitledBorder(Messages.getString("Proxy.BORDER_TITLE"),
                CometeTitledBorder.DEFAULT_JUSTIFICATION, CometeTitledBorder.DEFAULT_POSITION, TITLE_FONT,
                DataFitterBean.TITLE_COLOR);
        border.setLineColor(DataFitterBean.BORDER_COLOR);
        setBorder(border);

    }

    @Override
    public String getToolTipText(MouseEvent event) {
        return CometeTitledBorderToolTipManager.getToolTipText(event, super.getToolTipText(event));
    }

    @Override
    public void setModel(String model) {
        super.setModel(model);
        start();
    }

    @Override
    protected void clearGUI() {
        cleanWidget(getSigmaRead());
        cleanWidget(getSigmaWrite());
        cleanWidget(getXRead());
        cleanWidget(getXWrite());
        cleanWidget(getXWriteCombo());
        cleanWidget(getYRead());
        cleanWidget(getYWrite());
        cleanWidget(getYWriteCombo());
    }

    /*
     * @see fr.soleil.bean.AbstractBean#onConnectionError()
     */
    @Override
    protected void onConnectionError() {
        DataFitterBean.LOGGER.info("Proxy component, Connection error.");
    }

    @Override
    protected void refreshGUI() {
        // System.out.println("model=" + model);
        if (model != null && !model.isEmpty()) {
            stringBox.connectWidget(getXWrite(), generateWriteAttributeKey(Proxy.NAME_X_ATTRIBUTE));
            // setWidgetModel(getXWrite(), stringBox,
            // generateWriteAttributeKey(Proxy.NAME_X_ATTRIBUTE));
            stringBox.connectWidget(getXWriteCombo(), generateWriteAttributeKey(Proxy.NAME_X_ATTRIBUTE));
            // setWidgetModel(getXWriteCombo(), stringBox,
            // generateWriteAttributeKey(Proxy.NAME_X_ATTRIBUTE));
            stringBox.connectWidget(getXRead(), generateWriteAttributeKey(Proxy.NAME_X_ATTRIBUTE));
            // setWidgetModel(getXRead(), stringBox,
            // generateWriteAttributeKey(Proxy.NAME_X_ATTRIBUTE));

            stringBox.connectWidget(getYWrite(), generateWriteAttributeKey(Proxy.NAME_Y_ATTRIBUTE));
            // setWidgetModel(getYWrite(), stringBox,
            // generateWriteAttributeKey(Proxy.NAME_Y_ATTRIBUTE));
            stringBox.connectWidget(getYWriteCombo(), generateWriteAttributeKey(Proxy.NAME_Y_ATTRIBUTE));
            // setWidgetModel(getYWriteCombo(), stringBox,
            // generateWriteAttributeKey(Proxy.NAME_Y_ATTRIBUTE));
            stringBox.connectWidget(getYRead(), generateWriteAttributeKey(Proxy.NAME_Y_ATTRIBUTE));
            // setWidgetModel(getYRead(), stringBox,
            // generateWriteAttributeKey(Proxy.NAME_Y_ATTRIBUTE));
            stringBox.connectWidget(getSigmaRead(), generateWriteAttributeKey(Proxy.NAME_SIGMA_ATTRIBUTE));
            // setWidgetModel(getSigmaRead(), stringBox,
            // generateWriteAttributeKey(Proxy.NAME_SIGMA_ATTRIBUTE));
            stringBox.connectWidget(getSigmaWrite(), generateWriteAttributeKey(Proxy.NAME_SIGMA_ATTRIBUTE));
            // setWidgetModel(getSigmaWrite(), stringBox,
            // generateWriteAttributeKey(Proxy.NAME_SIGMA_ATTRIBUTE));
        }
    }

    /**
     * This method initializes xWrite
     * 
     * @return fr.esrf.tangoatk.widget.attribute.StringScalarEditor
     */
    private TextField getXWrite() {
        if (xWrite == null) {
            xWrite = new TextField();
            xWrite.setToolTipText(X_INFO);
            stringBox.setColorEnabled(xWrite, false);
        }
        return xWrite;
    }

    /**
     * This method initializes xWrite
     * 
     * @return fr.esrf.tangoatk.widget.attribute.StringScalarEditor
     */
    private ComboBox getXWriteCombo() {
        if (xProxiesCombo == null) {
            xProxiesCombo = new ComboBox();
            xProxiesCombo.setVisible(false);
            xProxiesCombo.setToolTipText(X_INFO);
        }
        return xProxiesCombo;
    }

    /**
     * This method initializes xWrite
     * 
     * @return fr.esrf.tangoatk.widget.attribute.StringScalarEditor
     */
    private ComboBox getYWriteCombo() {
        if (yProxiesCombo == null) {
            yProxiesCombo = new ComboBox();
            yProxiesCombo.setVisible(false);
            yProxiesCombo.setToolTipText(Y_INFO);
        }
        return yProxiesCombo;
    }

    /**
     * This method initializes xRead
     * 
     * @return fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer
     */
    private Label getXRead() {
        if (xRead == null) {
            xRead = generateLabel();
            xRead.setToolTipText(X_INFO);
        }
        return xRead;
    }

    /**
     * This method initializes yWrite
     * 
     * @return fr.esrf.tangoatk.widget.attribute.StringScalarEditor
     */
    private TextField getYWrite() {
        if (yWrite == null) {
            yWrite = new TextField();
            yWrite.setToolTipText(Y_INFO);
            stringBox.setColorEnabled(yWrite, false);
        }
        return yWrite;
    }

    /**
     * This method initializes yRead
     * 
     * @return fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer
     */
    private Label getYRead() {
        if (yRead == null) {
            yRead = generateLabel();
            yRead.setToolTipText(Y_INFO);
        }
        return yRead;
    }

    /**
     * This method initializes sigmaWrite
     * 
     * @return fr.esrf.tangoatk.widget.attribute.StringScalarEditor
     */
    private TextField getSigmaWrite() {
        if (sigmaWrite == null) {
            sigmaWrite = new TextField();
            sigmaWrite.setToolTipText(SIGMA_INFO);
            stringBox.setColorEnabled(sigmaWrite, false);
        }
        return sigmaWrite;
    }

    /**
     * This method initializes sigmaRead
     * 
     * @return fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer
     */
    private Label getSigmaRead() {
        if (sigmaRead == null) {
            sigmaRead = generateLabel();
            sigmaRead.setToolTipText(SIGMA_INFO);
        }
        return sigmaRead;
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    @Override
    public void start() {
        refreshGUI();
        // super.start();
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame(Proxy.class.getSimpleName() + (args != null && args.length > 0 ? " " + args[0] : ""));

        JTabbedPane panel = new JTabbedPane();
        final Proxy bean = new Proxy();
        bean.setXProxiesChoice(new String[] { "test/scan/julien/actuator_1_1", "test/scan/julien/actuator_2_1" });
        bean.setYProxiesChoice(new String[] { "test/scan/julien/data_0_1", "test/scan/julien/data_0_2" });
        bean.setModel(args[0]);

        final Proxy bean2 = new Proxy();
        bean2.setXProxiesChoice(new String[] { "test/scan/julien/actuator_1_1", "test/scan/julien/actuator_2_1" });
        bean2.setYProxiesChoice(new String[] { "test/scan/julien/data_0_1", "test/scan/julien/data_0_2" });
        bean2.setModel(args[0]);

        panel.add("bean1", bean);
        panel.add("bean2", bean2);

        bean.start();
        bean2.start();
        bean.stop();

        bean.start();
        bean2.start();
        bean.start();
        bean.start();
        bean.stop();
        bean.start();

        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(panel, BorderLayout.CENTER);
        final JButton startButton = new JButton("start");
        startButton.addActionListener(e -> {
            SwingUtilities.invokeLater(() -> {
                if (bean.stopped) {
                    startButton.setText("stop");
                    bean.start();
                } else {
                    startButton.setText("start");
                    bean.stop();
                }
            });
        });
        mainPanel.add(startButton, BorderLayout.SOUTH);
        frame.setTitle(Messages.getString("DataFitter.MAINFRAME_TITLE"));
        frame.setContentPane(mainPanel);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}