/**
 *
 */
package fr.soleil.comete.bean.datafitter;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.prefs.Preferences;

import javax.swing.JLabel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import fr.soleil.comete.box.AbstractTangoBox;
import fr.soleil.comete.swing.Label;
import fr.soleil.comete.swing.TextField;

/**
 * @author HARDION
 * 
 */
public class Quality extends AbstractTangoBox {

    private static final long serialVersionUID = 6933912083549861135L;

    private JLabel nbIterationsLabel = null;
    private TextField nbIterationsViewer = null;
    protected final static String NB_ITER_INFO = Messages.getString("Quality.NBITER_INFO");
    protected final static String NB_ITER_TITLE = Messages.getString("Quality.NBITER_TITLE");
    private final static String ITERATIONS_ATTRIBUTE = "nbIterations";

    private JLabel determinationLabel = null;
    private Label determinationViewer = null;
    protected final static String DETERMINATION_INFO = Messages.getString("Quality.DETERMINATION_INFO");
    protected final static String DETERMINATION_TITLE = Messages.getString("Quality.DETERMINATION_TITLE");
    private final static String DETERMINATION_ATTRIBUTE = "determinationQualityFactor";

    private JLabel statisticLabel = null;
    private TextField statisticViewer = null;
    protected final static String STATISTIC_INFO = Messages.getString("Quality.STATISTIC_INFO");
    protected final static String STATISTIC_TITLE = Messages.getString("Quality.STATISTIC_TITLE");
    private final static String STATISTIC_ATTRIBUTE = "fStatisticQualityFactor";

    /**
     * This method initializes
     * 
     */
    public Quality() {
        super();
        initialize();
    }

    /**
     * This method initializes this
     * 
     */
    private void initialize() {

        GridBagConstraints nbIterationsLabelConstraints = new GridBagConstraints();
        nbIterationsLabelConstraints.fill = GridBagConstraints.BOTH;
        nbIterationsLabelConstraints.gridx = 0;
        nbIterationsLabelConstraints.gridy = 0;
        GridBagConstraints nbIterationsViewerConstraints = new GridBagConstraints();
        nbIterationsViewerConstraints.gridx = 1;
        nbIterationsViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        nbIterationsViewerConstraints.gridy = 0;
        nbIterationsViewerConstraints.weightx = 1;

        GridBagConstraints determinationLabelConstraints = new GridBagConstraints();
        determinationLabelConstraints.fill = GridBagConstraints.BOTH;
        determinationLabelConstraints.gridx = 0;
        determinationLabelConstraints.gridy = 1;
        GridBagConstraints determinationViewerConstraints = new GridBagConstraints();
        determinationViewerConstraints.gridx = 1;
        determinationViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        determinationViewerConstraints.gridy = 1;
        determinationViewerConstraints.weightx = 1;

        GridBagConstraints statisticLabelConstraints = new GridBagConstraints();
        statisticLabelConstraints.fill = GridBagConstraints.BOTH;
        statisticLabelConstraints.gridx = 0;
        statisticLabelConstraints.gridy = 2;
        GridBagConstraints statisticViewerConstraints = new GridBagConstraints();
        statisticViewerConstraints.gridx = 1;
        statisticViewerConstraints.fill = GridBagConstraints.HORIZONTAL;
        statisticViewerConstraints.gridy = 2;
        statisticViewerConstraints.weightx = 1;

        nbIterationsLabel = new JLabel();
        nbIterationsLabel.setHorizontalAlignment(JLabel.CENTER);
        nbIterationsLabel.setText(NB_ITER_TITLE);
        nbIterationsLabel.setToolTipText(NB_ITER_INFO);

        determinationLabel = new JLabel();
        determinationLabel.setHorizontalAlignment(JLabel.CENTER);
        determinationLabel.setText(DETERMINATION_TITLE);
        determinationLabel.setToolTipText(DETERMINATION_INFO);

        statisticLabel = new JLabel();
        statisticLabel.setHorizontalAlignment(JLabel.CENTER);
        statisticLabel.setText(STATISTIC_TITLE);
        statisticLabel.setToolTipText(STATISTIC_INFO);

        this.setLayout(new GridBagLayout());
        this.setSize(new Dimension(235, 135));

        this.add(nbIterationsLabel, nbIterationsLabelConstraints);
        this.add(getNbIterationsViewer(), nbIterationsViewerConstraints);

        this.add(determinationLabel, determinationLabelConstraints);
        this.add(getDeterminationViewer(), determinationViewerConstraints);

        this.add(statisticLabel, statisticLabelConstraints);
        this.add(getStatisticViewer(), statisticViewerConstraints);

        this.setBorder(new TitledBorder(new LineBorder(DataFitterBean.BORDER_COLOR, 1),
                Messages.getString("Quality.BORDER_TITLE"), TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, new Font("Dialog", Font.BOLD, 12), DataFitterBean.TITLE_COLOR));

    }

    @Override
    protected void clearGUI() {
        cleanWidget(getDeterminationViewer());
        cleanWidget(getNbIterationsViewer());
        cleanWidget(getStatisticViewer());
    }

    @Override
    protected void onConnectionError() {
        DataFitterBean.LOGGER.info("Quality component, Connection error.");
    }

    @Override
    protected void refreshGUI() {
        setWidgetModel(getNbIterationsViewer(), stringBox, generateAttributeKey(Quality.ITERATIONS_ATTRIBUTE));
        setWidgetModel(getDeterminationViewer(), stringBox, generateAttributeKey(Quality.DETERMINATION_ATTRIBUTE));
        setWidgetModel(getStatisticViewer(), stringBox, generateAttributeKey(Quality.STATISTIC_ATTRIBUTE));
    }

    public TextField getNbIterationsViewer() {
        if (nbIterationsViewer == null) {
            nbIterationsViewer = generateTextField();
            nbIterationsViewer.setToolTipText(NB_ITER_INFO);
            stringBox.setReadOnly(nbIterationsViewer, true);
        }
        return nbIterationsViewer;
    }

    public Label getDeterminationViewer() {
        if (determinationViewer == null) {
            determinationViewer = generateLabel();
            determinationViewer.setToolTipText(DETERMINATION_INFO);
        }
        return determinationViewer;
    }

    public TextField getStatisticViewer() {
        if (statisticViewer == null) {
            statisticViewer = generateTextField();
            statisticViewer.setToolTipText(STATISTIC_INFO);
            stringBox.setReadOnly(statisticViewer, false);
        }
        return statisticViewer;
    }

    @Override
    protected void savePreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

    @Override
    protected void loadPreferences(Preferences preferences) {
        // TODO Auto-generated method stub

    }

}